## SAGARAHEPH

SaGarahePH is a new service in Davao City that offers its business patrons a place where they can spend their time to relax and network with other business people. It is a safe haven built for men. 

The website will use Laravel as the framework for the site

- Laravel - https://laravel.com/
- Laravel Installation - https://laravel.com/docs/7.x
- To run Laravel you can use Homestead or any other server software of your choice.

- Bootstrap - https://getbootstrap.com/

Create an env file by copying the .env.example file and fill in your database connection

- Run `composer install`
- Run `php artisan key:generate`
- Run `php artisan migrate` and `php artisan db:seed --class=UserSeeder`
  to populate your db