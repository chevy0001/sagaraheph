<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('birthday');
            $table->string('job_title')->nullable();;
            $table->string('email')->unique();
            $table->string('phone_number');
            $table->string('country')->nullable();;
            $table->string('region')->nullable();;
            $table->string('city')->nullable();;
            $table->string('barangay')->nullable();;
            $table->string('street')->nullable();;
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('profile_photo')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->string('role')->nullable();;
            $table->string('token_id');
            $table->string('is_delete');
            $table->softDeletes();
            $table->index('user_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
