<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

         $users =[ 
         [
            'firstname' => 'admin',
            'lastname' => 'admin',
            'birthday'=>'01-01-1990',
            'email' => 'admin@admin.com',
            'phone_number' => '09999999999',
            'job_title'=>'Racer',
            'country'=>'PH',
            'region'=>'Davao del Sur',
            'city'=>'Davao',
            'barangay'=>'Toril',
            'street'=>'Japan St',
            'role'=>'1',
            'password' => Hash::make('password'),
            'token_id'=>Str::random(10),
            'is_delete'=>'0',
        ],

        [
            'firstname' => 'user',
            'lastname' => 'user',
            'birthday'=>'01-01-1989',
            'email' => 'user@user.com',
            'phone_number' => '09999999998',
            'job_title'=>'Racer',
            'country'=>'PH',
            'region'=>'Davao del Sur',
            'city'=>'Davao',
            'barangay'=>'Toril',
            'street'=>'Japan St',
            'role'=>'0',
            'password' => Hash::make('password'),
            'token_id'=>Str::random(10),
            'is_delete'=>'0',
        ]

    ];
    User::insert($users);
    }
}
