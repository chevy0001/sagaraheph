@extends('layouts.app')

@section('content')
<!-- Hero Section Begin -->
<section class="hero-section">
    <div class="setBg">
        <div class="hero-content">
            <div class="container text-center">

                <div class="hi-text">
                    <h1 class="page-title font-weight-bold text-uppercase mb-4">
                        Sa Garahe
                    </h1>
                    <h3 class="font-weight-lighter page-description text-uppercase">
                         Men's Safe Haven
                    </h3>
                    <a href="/get-quote" class="primary-btn btn-normal">Get Quote</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ChoseUs Section Begin -->
<section class="choseus-section spad">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <span class="title-description">Why choose us</span>
                    <h1 class="mt-2">Exclusive and Limited</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="cs-item">
                    <div class="icon mb-4">
                        <svg enable-background="new 0 0 82 82" version="1.1" viewBox="0 0 82 82" xml:space="preserve"
                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="82" height="82">
                            <defs>
                                <rect id="b" width="82" height="82"/>
                            </defs>
                            <clipPath id="a">
                                <use xlink:href="#b"/>
                            </clipPath>
                            <g class="featureA0">
                                <path class="featureA1" d="m81.1 13.7-39.7-13.6c-0.3-0.1-0.6-0.1-0.9 0l-39.6 13.6c-0.5 0.2-0.9 0.8-0.9 1.3v65.6c0 0.8 0.6 1.4 1.4 1.4h6.8c0.8 0 1.4-0.6 1.4-1.4v-42.3h62.9v42.4c0 0.8 0.6 1.4 1.4 1.4h6.8c0.8 0 1.4-0.6 1.4-1.4v-65.7c-0.1-0.5-0.5-1.1-1-1.3zm-74.3 23.2v42.4h-4.1v-52h4.1v9.6zm65.6-1.4h-62.8v-2.7h62.9v2.7zm0-5.4h-62.8v-2.7h62.9v2.7zm6.9 49.2h-4.1v-52h4.1v52zm0-54.7h-76.6v-8.6l38.3-13.2 38.3 13.2v8.6z"/>
                                <path class="featureA1" d="m67.6 58.9-5.1-2.5-4.5-9c-0.7-1.4-2.1-2.3-3.7-2.3h-26.6c-1.6 0-3 0.9-3.7 2.3l-4.5 9-5.1 2.5c-0.5 0.2-0.8 0.7-0.8 1.2v19.1c0 1.5 1.2 2.7 2.7 2.7h5.5c1.5 0 2.7-1.2 2.7-2.7v-4.1h32.8v4.1c0 1.5 1.2 2.7 2.7 2.7h5.5c1.5 0 2.7-1.2 2.7-2.7v-19.1c0.1-0.5-0.2-1-0.6-1.2zm-41.2-10.3c0.2-0.5 0.7-0.8 1.2-0.8h26.7c0.5 0 1 0.3 1.2 0.8l3.7 7.4h-36.5l3.7-7.4zm-4.5 30.7h-5.5v-4.1h5.5v4.1zm43.7 0h-5.5v-4.1h5.5v4.1zm0-6.9h-49.2v-11.4l4.4-2.2h40.4l4.4 2.2v11.4z"/>
                                <path class="featureA1" d="m26 61.5h-4.1c-1.5 0-2.7 1.2-2.7 2.7v2.8c0 1.5 1.2 2.7 2.7 2.7h4.1c1.5 0 2.7-1.2 2.7-2.7v-2.7c0-1.6-1.2-2.8-2.7-2.8zm0 5.5h-4.1v-2.7h4.1v2.7z"/>
                                <path class="featureA1" d="m60.1 61.5h-4.1c-1.5 0-2.7 1.2-2.7 2.7v2.8c0 1.5 1.2 2.7 2.7 2.7h4.1c1.5 0 2.7-1.2 2.7-2.7v-2.7c0.1-1.6-1.2-2.8-2.7-2.8zm0 5.5h-4.1v-2.7h4.1v2.7z"/>
                                <path class="featureA1" d="m47.8 61.5h-13.6c-1.5 0-2.7 1.2-2.7 2.7v2.8c0 1.5 1.2 2.7 2.7 2.7h13.7c1.5 0 2.7-1.2 2.7-2.7v-2.7c0-1.6-1.3-2.8-2.8-2.8zm0 5.5h-13.6v-2.7h13.7v2.7z"/>
                            </g>
                        </svg>
                    </div>
                    <h6>Wash and fix</h6>
                    <p>
                        Experience high quality carwash services.
                        Spare parts and tools are available for your convenience.
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="cs-item">
                    <div class="icon mb-4">
                        <svg enable-background="new 0 0 82 82" version="1.1" viewBox="0 0 82 82" xml:space="preserve"
                            xmlns="http://www.w3.org/2000/svg" width="82" height="82">
                            <path class="featureA1" d="m63.6 43.7-8.9-11.3c3.1-3.2 5.1-7.5 5.4-12h13.7v1.6c-3.2 0.7-5.5 3.4-5.5 6.7 0 3.8 3.1 6.8 6.8 6.8 3.8 0 6.8-3.1 6.8-6.8 0-3.2-2.3-6-5.5-6.7v-2.9c0-0.8-0.6-1.4-1.4-1.4h-14.9c-0.3-3.8-1.7-7.4-4-10.4l1.9-1.8h16c0.8 2.1 3.1 3.3 5.2 2.5s3.3-3.1 2.5-5.2-3.1-3.3-5.2-2.5c-1.2 0.4-2.1 1.3-2.5 2.5h-16.6c-0.4 0-0.7 0.1-1 0.4l-2.2 2.2c-7.6-7.3-19.7-7.1-27 0.5-3.1 3.2-5 7.4-5.3 11.9h-11.4l-4.5-10.1c2-1.1 2.8-3.5 1.7-5.5s-3.5-2.8-5.5-1.7c-2 1-2.8 3.5-1.7 5.5 0.5 1.1 1.5 1.8 2.7 2.1l5.2 11.6c0.2 0.5 0.7 0.8 1.2 0.8h12.4c0.3 4.4 2.2 8.6 5.3 11.9l-11.6 11.5c-7.5-1.1-14.4 4-15.5 11.5s4 14.4 11.5 15.5c0.2 0 0.5 0.1 0.7 0.1v6.9c0 0.8 0.6 1.4 1.4 1.4h12.5c0.8 2.1 3.1 3.3 5.2 2.5s3.3-3.1 2.5-5.2-3.1-3.3-5.2-2.5c-1.2 0.4-2.1 1.3-2.5 2.5h-11.3v-5.6c7.5-0.7 13-7.4 12.2-14.9-0.5-5.1-3.8-9.4-8.5-11.3l10.5-10.5c6.9 5.4 16.5 5.4 23.4 0.1l7.6 9.6c-10.5 1.4-17.8 11-16.3 21.5s11.1 17.8 21.6 16.3c10.5-1.4 17.8-11.1 16.3-21.6-1.3-9.2-9-16.1-18.2-16.5zm15.6-15c0 2.3-1.8 4.1-4.1 4.1s-4.1-1.8-4.1-4.1 1.8-4.1 4.1-4.1 4.1 1.8 4.1 4.1zm-1.3-26c0.8 0 1.4 0.6 1.4 1.4s-0.6 1.4-1.4 1.4-1.4-0.6-1.4-1.4 0.6-1.4 1.4-1.4zm-73.8 2.8c-0.8 0-1.4-0.6-1.4-1.4s0.6-1.4 1.4-1.4 1.4 0.6 1.4 1.4-0.6 1.4-1.4 1.4zm26 71c0.8 0 1.4 0.6 1.4 1.4s-0.6 1.4-1.4 1.4-1.4-0.6-1.4-1.4 0.6-1.4 1.4-1.4zm-11-9.7c-3.4 2-7.6 2-10.9 0v-2.6c0-3 2.4-5.5 5.5-5.5s5.5 2.4 5.5 5.5v2.6zm-8.2-13.5c0-1.5 1.2-2.7 2.7-2.7s2.7 1.2 2.7 2.7-1.1 2.7-2.6 2.7-2.8-1.2-2.8-2.7zm11-3.2c1.8 2 2.7 4.6 2.7 7.3s-1 5.2-2.7 7.2v-0.4c0-3-1.6-5.7-4.2-7.2 2.1-2.2 2-5.6-0.2-7.7s-5.6-2-7.7 0.2c-2 2.1-2 5.4 0 7.5-2.6 1.4-4.2 4.2-4.2 7.2v0.4c-4-4.5-3.6-11.4 1-15.4 4.4-4 11.3-3.6 15.3 0.9zm28.7-17.7c-5.7 4.1-13.4 4.1-19.1 0v-3.3c0-2.5 2-4.5 4.5-4.5h3l-2 5c-0.2 0.5-0.1 1.1 0.3 1.5l2.7 2.7c0.5 0.5 1.4 0.5 1.9 0l2.7-2.7c0.4-0.4 0.5-1 0.3-1.5l-2-5h3c2.5 0 4.5 2 4.5 4.5v3.3zm-9.4-22.2c-0.1 0.1-0.1 0.2-0.1 0.3-0.7 2.1-4.4 2.9-6.8 3.1 0.3-4.6 2.5-6.8 6.7-6.8 3.9 0 6 1.8 6.6 5.7l-4.6-2.7c-0.6-0.4-1.4-0.2-1.8 0.4zm1.7 2.6 4.9 2.9c-0.5 3.3-4.7 6.1-6.8 6.1-3-0.5-5.6-2.6-6.6-5.5 2.3-0.2 6.5-0.9 8.5-3.5zm-0.8 16.9-1.1 1.2-1.1-1.1 1.1-2.8 1.1 2.7zm4.9-7.8c2.1-1.7 3.4-4.2 3.6-6.9 0-9-5.2-10.9-9.6-10.9s-9.6 1.9-9.6 10.9c0.2 2.7 1.5 5.2 3.6 6.9-3.6 0.4-6.3 3.5-6.3 7.2v0.8c-6-6.8-5.3-17.1 1.5-23.1s17.1-5.3 23.1 1.5c5.5 6.2 5.5 15.5 0 21.7v-0.8c0-3.8-2.7-6.8-6.3-7.3zm25.4 54.2c-5.7 4.1-13.4 4.1-19.1 0v-3.3c0-2.5 2-4.5 4.5-4.5h3l-2 5c-0.2 0.5-0.1 1.1 0.3 1.5l2.7 2.7c0.5 0.5 1.4 0.5 1.9 0l2.7-2.7c0.4-0.4 0.5-1 0.3-1.5l-2-5h3c2.5 0 4.5 2 4.5 4.5v3.3zm-7.7-19.6 4.9 2.9c-0.5 3.3-4.7 6.1-6.8 6.1-3-0.5-5.6-2.6-6.6-5.5 2.4-0.2 6.6-0.9 8.5-3.5zm-8.6 0.8c0.4-4.6 2.5-6.8 6.8-6.8 3.9 0 6 1.8 6.6 5.7l-4.6-2.7c-0.6-0.4-1.5-0.2-1.9 0.5 0 0 0 0.1-0.1 0.2-0.7 2.1-4.4 2.9-6.8 3.1zm7.9 16.1-1.1 1.1-1.1-1.1 1.1-2.8 1.1 2.8zm11.1 0.2v-0.8c0-3.7-2.7-6.7-6.3-7.2 2.1-1.7 3.4-4.2 3.6-6.9 0-9-5.2-10.9-9.6-10.9s-9.6 1.9-9.6 10.9c0.2 2.7 1.5 5.2 3.6 6.9-3.6 0.4-6.3 3.5-6.3 7.2v0.8c-6-6.8-5.3-17.1 1.5-23.1s17.1-5.3 23.1 1.5c2.6 3 4.1 6.8 4.1 10.8s-1.4 7.9-4.1 10.8z"/>
                        </svg>
                    </div>
                    <h6>Network with peers</h6>
                    <p>
                        Discover people with the same mindset and interest over coffee.
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="cs-item">
                    <div class="icon mb-4">
                        <svg enable-background="new 0 0 82 51" version="1.1" viewBox="0 0 82 51" xml:space="preserve"
                            xmlns="http://www.w3.org/2000/svg" width="82" height="82">
                            <path class="featureA1" d="m77.4 13.2h-3.3v-5.3c0-2.1-0.8-4.1-2.3-5.6s-3.6-2.3-5.7-2.3h-18.5c-1.3 0-2.6 0.3-3.8 0.9s-2.1 1.5-2.9 2.6c-0.7-1.1-1.7-2-2.9-2.6-1-0.6-2.3-0.9-3.6-0.9h-18.5c-2.1 0-4.1 0.8-5.6 2.3s-2.4 3.5-2.4 5.6v5.3h-3.3c-1.2 0-2.4 0.5-3.3 1.4s-1.4 2-1.4 3.3v25.8c0 0.4 0.1 0.7 0.4 0.9s0.7 0.4 1 0.4h2.7v4c0 0.4 0.1 0.7 0.4 0.9 0.2 0.2 0.6 0.4 0.9 0.4h4c0.3 0 0.6-0.1 0.8-0.3s0.4-0.4 0.5-0.7l1.1-4.3h58.8l1.1 4.3c0.1 0.3 0.2 0.5 0.5 0.7 0.2 0.2 0.5 0.3 0.8 0.3h4c0.4 0 0.7-0.1 0.9-0.4 0.2-0.2 0.4-0.6 0.4-0.9v-4h2.6c0.4 0 0.7-0.1 0.9-0.4 0.2-0.2 0.4-0.6 0.4-0.9v-25.8c0-1.2-0.5-2.4-1.4-3.3s-2.1-1.4-3.3-1.4zm-69.2 34.4h-1.6v-2.6h2.3l-0.7 2.6zm5-5.3h-10.6v-24.4c0-0.5 0.2-1 0.6-1.4s0.9-0.6 1.4-0.6h6.6c0.5 0 1 0.2 1.4 0.6s0.6 0.9 0.6 1.4v24.4zm-2-29.1h-0.7v-5.3c0-1.4 0.6-2.7 1.6-3.7s2.3-1.5 3.7-1.6h18.5c1.4 0 2.7 0.6 3.7 1.6s1.5 2.3 1.6 3.7v18.5h-18.4c-2 0-3.8 0.7-5.3 2v-10.5c0-1.2-0.5-2.4-1.4-3.3s-2-1.4-3.3-1.4zm54.9 29.1h-50.2v-7.9c0-1.4 0.6-2.7 1.6-3.7s2.3-1.5 3.7-1.6h39.7c1.4 0 2.7 0.6 3.7 1.6s1.5 2.3 1.6 3.7v7.9zm0-24.4v10.6c-1.5-1.3-3.3-2-5.3-2h-18.5v-18.6c0-1.4 0.6-2.7 1.6-3.7s2.3-1.5 3.7-1.6h18.5c1.4 0 2.7 0.6 3.7 1.6s1.5 2.3 1.6 3.7v5.3h-0.7c-1.2 0-2.4 0.5-3.3 1.4-0.8 0.9-1.3 2-1.3 3.3zm9.3 29.7h-1.6l-0.7-2.6h2.3v2.6zm4-5.3h-10.6v-24.4c0-0.5 0.2-1 0.6-1.4s0.9-0.6 1.4-0.6h6.6c0.5 0 1 0.2 1.4 0.6s0.6 0.9 0.6 1.4v24.4z"/>
                            <path class="featureA1" d="m25.1 10.6c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3 0.6 1.3 1.3 1.3z"/>
                            <path class="featureA1" d="m25.1 18.5c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3 0.6 1.3 1.3 1.3z"/>
                            <path class="featureA1" d="m29.1 14.5c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3c0 0.8 0.6 1.3 1.3 1.3z"/>
                            <path class="featureA1" d="m21.2 14.5c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3c-0.1 0.8 0.5 1.3 1.3 1.3z"/>
                            <path class="featureA1" d="m56.9 10.6c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3 0.5 1.3 1.3 1.3z"/>
                            <path class="featureA1" d="m56.9 18.5c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3c-0.1 0.7 0.5 1.3 1.3 1.3z"/>
                            <path class="featureA1" d="m60.8 14.5c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3c0 0.8 0.6 1.3 1.3 1.3z"/>
                            <path class="featureA1" d="m52.9 14.5c0.7 0 1.3-0.6 1.3-1.3s-0.6-1.3-1.3-1.3-1.3 0.6-1.3 1.3c0 0.8 0.6 1.3 1.3 1.3z"/>
                        </svg>
                    </div>
                    <h6>Chill and meditate</h6>
                    <p>
                        Your own exclusive VIP room is waiting for you to empty your
                        mind from stress and be with yourself.
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="cs-item">
                    <div class="icon mb-4">
                        <svg enable-background="new 0 0 82 92" version="1.1" viewBox="0 0 82 92" xml:space="preserve"
                            xmlns="http://www.w3.org/2000/svg" width="82" height="82">
                            <path class="featureA1" d="m80.2 21.4h-33.9v-19.6c0-1-0.8-1.8-1.8-1.8h-42.7c-1 0-1.8 0.8-1.8 1.8v87.7c0 1 0.8 1.8 1.8 1.8h78.5c1 0 1.8-0.8 1.8-1.8v-66.3c-0.1-1-0.9-1.8-1.9-1.8zm-33.9 3.6h32.1v44.9h-32.1v-44.9zm-28.5 62.7v-14.3h10.7v14.3h-10.7zm12.5-17.8h-14.3c-1 0-1.8 0.8-1.8 1.8v16h-10.6v-84.1h39.2v84.1h-10.7v-16c0-1-0.8-1.8-1.8-1.8zm16 17.8v-14.3h32.1v14.3h-32.1z"/>
                            <path class="featureA1" d="m21.4 14.3h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m21.4 26.7h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.7c0 0.9 0.8 1.7 1.8 1.7z"/>
                            <path class="featureA1" d="m21.4 39.2h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m21.4 51.7h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m21.4 64.2h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m8.9 14.3h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m8.9 26.7h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.7c0 0.9 0.8 1.7 1.8 1.7z"/>
                            <path class="featureA1" d="m8.9 39.2h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m8.9 51.7h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m8.9 64.2h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8z"/>
                            <path class="featureA1" d="m37.4 7.1h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m37.4 19.6h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m37.4 32.1h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m37.4 44.6h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m37.4 57h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m70.4 32.1h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m70.4 44.6h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m70.4 57h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m57.9 32.1h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m57.9 44.6h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                            <path class="featureA1" d="m57.9 57h-3.6c-1 0-1.8 0.8-1.8 1.8v3.6c0 1 0.8 1.8 1.8 1.8h3.6c1 0 1.8-0.8 1.8-1.8v-3.6c0-1-0.8-1.8-1.8-1.8z"/>
                        </svg>
                    </div>
                    <h6>Business</h6>
                    <p>
                        Board rooms are available for Meetings,
                        Discussions and Conference.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ChoseUs Section End -->

<!-- Services Section Begin -->
<section class="classes-section spad">
    <div class="container-fluid">
        <div class="row mb-9">
            <div class="col-lg-12">
                <div class="section-title">
                    <h1 class="mb-3">Services we offer</h1>
                    <span class="sub-description">
                        If you don’t try this service, you will miss some opportunities of your lifetime.
                    </span>
                </div>
            </div>
        </div>
        <div class="row services">
            <div class="col-12 col-md-6 px-0 image img-container">
                <img src="{{ asset('images/services-a.jpg') }}" class="featured-img">
            </div>
            <div class="col-12 col-md-6 description">
                <h1>Amazing Garahe Setup</h1>
                <p>
                    Aside from quality carwash service  Sa Garahe have complete set of spare parts, tools and best staff that will cater to your need. 


                </p>
                <a href="/get-quote" class="secondary-btn btn-normal mb-5">Get Quote</a>
            </div>
        </div>
        <div class="row services">
            <div class="col-12 col-md-6 description">
                <h1>Relax and Meditate</h1>
                <p>
                    Sa Garahe is designed to provide a relaxation set up not just for your car but a place that you can concentrate and plan for your next move.
                </p>
                <a href="/get-quote" class="secondary-btn btn-normal mb-5">Get Quote</a>
            </div>
            <div class="col-12 col-md-6 px-0 image img-container">
                <img src="{{ asset('images/services-b.jpg') }}" class="featured-img">
            </div>
        </div>
        <div class="row services">
            <div class="col-12 col-md-6 px-0 image img-container">
                <img src="{{ asset('images/services-c.jpg') }}" class="featured-img">
            </div>
            <div class="col-12 col-md-6 description">
                <h1>Chill and Meet</h1>
                <p>
                    Meet great people while having coffee, take advantage of this 
                    rare opportunity since members are no ordinary customers but 
                    with great minds and full of ambitions towards success.
                </p>
                <a href="/get-quote" class="secondary-btn btn-normal">Get Quote</a>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->

<!-- Banner Section Begin -->
<section class="banner-section">
    <div class="banner-bg">
        <div class="row">
            <div class="col-12 col-md-6 flex-grow-1 mb-4">
                <h1>Ready to get started?</h1>
                <p>Get a free quote or talk to us to get more deals.</p>
            </div>
            <div class="col-12 col-md-6 banner-buttons" id="freeqoute">
                <a href="/get-quote" class="primary-btn btn-normal mb-3 mr-lg-1" role="button">Get a quote</a>
                <a href="/contact" class="secondary-btn btn-normal mb-3 ml-lg-1" role="button">Contact us</a>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

<!-- Pricing Section Begin -->
<section class="pricing-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <span class="title-description">Our Plans</span>
                    <h1 class="mt-2">Choose your plan</h1>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Regular</h3>
                    <div class="pi-price">
                        <h2>₱15,000</h2>
                        <span>5 Seats</span><br/><br/>
                        <span>3 Tables</span><br/>
                        {{-- <span>(5 Seats/tables)</span> --}}
                    </div>
                    <ul>
                        <li>5 Day Pass a week to your allocated</li>
                        <li>Seat and table</li>
                        <li>Car wash and clean</li>
                        <li>Reshuffle every 3 weeks</li>
                    </ul>
                    <a href="/get-quote" class="secondary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Elite</h3>
                    <div class="pi-price">
                        <h2>₱60,000</h2>
                        <span>2 Seats</span><br/><br/>
                        <span>2 Rooms</span>
                    </div>
                    <ul>
                        <li>Room Butler Service</li>
                        <li>25% Discount to all orders</li>
                        <li>5 days pass for Elite Rooms and 2 days</li>
                        <li>Pass for Elite Cubicles a week</li>
                        <li>Car wash and clean</li>
                    </ul>
                    <a href="/get-quote" class="primary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Vip</h3>
                    <div class="pi-price">
                        <h2>₱45,000</h2>
                        <span>3 Seats</span><br/><br/>
                        <span>3 Rooms</span>
                    </div>
                    <ul>
                        <li>Car wash and clean</li>
                        <li>5 days pass for VIP Rooms and 2 days</li>
                        <li>Pass for VIP Cubicles a week</li>
                        <li>Car wash and clean</li>
                    </ul>
                    <a href="/get-quote" class="secondary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Pricing Section End -->

<!-- Blog Section Begin -->
{{-- 
<section class="team-section mb-10">
    <div class="blog-container">
        <div class="container">
            <div class="row featured-blog mb-4">
                <div class="col-12 col-md-4 px-0 blog-img-container">
                    <img src="{{ asset('images/blog-img.png') }}" class="blog-featured-img">
                </div>
                <div class="col-12 col-md-8">
                    <div class="description">
                        <span class="author">
                            Author Name
                        </span>
                        <h1 class="mb-3">
                            The best blog for SaGarahePH
                        </h1> 
                        <p class="mb-9">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore...
                        </p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 mb-4 pl-md-0">
                    <div class="blogs">
                        <span class="author">
                            Author Name
                        </span>
                        <h1 class="mb-3">
                            Keys to writing copy that actually converts and sells users
                        </h1> 
                        <p class="mb-9">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore...
                        </p>
                        <a href="#">Read More</a>
                    </div>
                </div>
                <div class="col-12 col-md-6 pr-md-0">
                    <div class="blogs">
                        <span class="author">
                            Author Name
                        </span>
                        <h1 class="mb-3">
                            Keys to writing copy that actually converts and sells users
                        </h1> 
                        <p class="mb-9">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore...
                        </p>
                        <a href="#">Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
--}}
<!-- Blog Section End -->

<!-- Contact Section Begin -->
<section class="banner-section">
    <div class="banner-bg">
        <div class="row">
            <div class="col-12 col-xl-4">
                <div class="gt-text">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/location.svg') }}">
                    </div>
                    <p class="mb-0">Alzate St. Bo. Obrero, <br/>Davao City, Philippines</p>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="gt-text">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/contact.svg') }}">
                    </div>
                    <p class="mb-0">(+63) 9953574834<br/> (082) 333-4203</p>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="gt-text email">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/email.svg') }}">
                    </div>
                    <p class="mb-0 pt-0">support@garahe.com</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Section End -->

<!-- Free Quote Section Begin -->
<section class="team-section spad">
    <div class="blog-container">
        <div class="container">
            <div class="row services">
                <div class="col-12 col-lg-6 mb-4">
                    <h1>Get a free quote</h1>
                    <p>
                        Provide your details in the form and click contact us. Then check your email for our reply. Thank you.
                    </p>
                    <p class="mb-0">Follow us</p>
                    <div class="fa-social">
                        <a href="www.facebook.com/sagaraheph"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-12 col-lg-6 px-0 free-quote-form">
                    @include('qoute');
                </div>
            </div>
        </div>
    </div>
    
</section>
<!-- Free Quote Section End -->
@endsection