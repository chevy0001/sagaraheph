@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">

              @if(auth()->user()->role === '1')
                @include('partials.admin-sidebar')
              @else
                @include('partials.user-sidebar')
              @endif
            </div>
            <div class="col-12 col-md-9 bg-white ">
              <div class="container-fluid p-md-5 pt-5 pb-5 mb-9">
                <div class="row">
                    <h3 class="flex-grow-1 justify-content-center">Notifications</h3>
                    <a href="/register-user" role="button" class="secondary-btn btn-normal">Register User</a>
                </div>
                <div class="row mt-4">
                  <div class="card">
                    <div class="card-header">
                        Users
                    </div>
                    <div class="card-body">
                      <table class="table table-hover user-list">
                        <thead>
                          <tr>
                              <th scope="col">ID</th>
                              <th scope="col">Notification Type</th>
                              <th scope="col">Message</th>
                              <th scope="col">Date</th>
                          </tr>
                        </thead>
                        <tbody>
                           @foreach($notifs as $notif)
                            
                            <tr>
                              <td>{{$notif->id}}</td>
                              <td><a href="{{$notif->link}}" style="text-decoration: none;">{{$notif->notif_type}}</a></td>
                              <td>{{$notif->message}}</td>
                              <td>{{$notif->created_at}}</td>
                            </tr>
                          
                           @endforeach 
                           {{ $notifs->links() }}
                        </tbody>
                      </table>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
