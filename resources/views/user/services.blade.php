@extends('layouts.client')

@section('content')
<section class="client-spad">
  <div class="container-fluid mt-9 mb-9">
    <div class="row">
      <div class="col-12 col-md-3">
          @include('partials.user-sidebar')
      </div>
      <div class="col-12 col-md-9 bg-white ">
        <div class="container-fluid p-md-5 pt-5 pb-5">
          <div class="row">
              <h3>Services</h3>
          </div>
          
          <div class="accordion" id="sgFAQ">
            <div class="card">
                <div class="card-header" id="faqOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Carwash
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="faqOne" data-parent="#sgFAQ" style="background: #ffffff !important">
                    <div class="card-body">
                       {{$linkqr}}
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                           Other Services
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="faqTwo" data-parent="#sgFAQ">
                    <div class="card-body">
                        Qr Code Here
                    </div>
                </div>
            </div>
          </div>  

        
        
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  

</script>

</section>
@endsection
