@extends('layouts.client')

@section('content')
<section class="client-spad">
  <div class="container-fluid mt-9 mb-9">
    <div class="row">
      <div class="col-12 col-md-3">
          @include('partials.user-sidebar')
      </div>
      <div class="col-12 col-md-9 bg-white ">
        <div class="container-fluid p-md-5 pt-5 pb-5">
          <div class="row">
              <h3>Dashboard</h3>
          </div>
          <div class="row">
            <div class="user-img d-flex align-item-center justify-content-center">
              <div class="profile-image">
                @if(auth()->user()->profile_photo) 
                  <img src="{{ asset('storage/'.auth()->user()->profile_photo) }}" style="border-radius: 50%;">
                @else
                  <img src="{{asset('images/default-img.svg') }}">
                @endif
              </div>
              {{--<a href="/edit-profile/{{auth()->user()->token_id}}" class="pl-3 pt-4">Edit Account</a>--}}
            </div>
            <table class="table mt-5">
              <tbody>
                <tr>
                    <td class="label">Full Name</td>
                    <td class="data">{{$profile->firstname ?? 'Your First Name'}} {{$profile->lastname ?? 'Your Last Name'}}</td>
                </tr>
                <tr>
                    <td class="label">Email</td>
                    <td class="data">{{$profile->email}}</td>
                </tr>
                <tr>
                    <td class="label">Date of Birth</td>
                    <td class="data">{{$profile->birthday}}</td>
                </tr>
                <tr>
                  <td class="label">Address</td>
                  <td class="data">{{$profile->street ?? ''}} {{$profile->barangay ?? ''}} {{$profile->city ?? ''}} {{$profile->region ?? ''}} {{$profile->country ?? ''}}</td>
                </tr>
                <tr>
                  <td class="label">Reset Password</td>
                  <td class="data"><a href="/password/reset">Reset Password</a></td>
                </tr>
                <tr>
                  <td>@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
@endif</td>
                </tr>
              </tbody>
            </table>
        </div>


        <div class="row mt-5 mb-3">
            <h3>Your Plan</h3>
        </div>
        <div class="row">
          <div class="card">
            <div class="card-header">
              @if($status)

                @if($status->upgrade_request == 1)
                  @if($status->status_name == "VIP")
                      VIP Membership
                  @elseif($status->status_name == "Elite")
                      Elite Membership
                  @elseif($status->status_name == "Regular") 
                    Regular Membership
                  @endif
                @else
                  {{$status->status_name}} Membership Pending
                @endif  
              @else
                Not Yet Registered 
              @endif 
            </div>
              <div class="card-body">
                <p class="card-title">Inclusions</p>
                

                @if($status)

                @if($status->upgrade_request == 1)
                  @if($status->status_name == "VIP")
              <p class="card-text mb-4">
                {{$status->seats}} Rooms/Seats<br/>
                20% Discount to all orders.<br/> 
                5 days pass for VIP Rooms and 2 days pass for VIP Cubicles a week.
                <br/>  
                Car wash and clean.</p>
                  @elseif($status->status_name == "Elite")
                  
                   <p class="card-text mb-4">
                    {{$status->seats}} Rooms/Seats<br/>
                    Room Butler Service<br/>
                    25% Discount to all orders<br/>
                    5 days pass for Elite Rooms and 2 days pass for Elite Cubicles a week 
                    <br/>
                    Car wash and clean<br/>

                   </p>   
                  @elseif($status->status_name == "Regular") 

                    <p class="card-text mb-4">
                      {{$status->seats}} Seats<br/>
                      5 Day Pass a week to your allocated seat and table.<br/>
                      Car wash and clean.<br/>
                      Reshuffle every 3 weeks
                    </p>
                  @endif
                @else
                  {{$status->status_name}} Membership Pending
                @endif  
              @else
                Not Yet Registered 
              @endif  


                <p class="card-title">Payment</p>
                <p class="card-text mb-4">
                    Your next bill for <strong>₱{{$status->total ?? '0.00'}}</strong> is on <strong>{{$nextPayment ?? ''}}</strong>.
                </p>
                <a href="/payment-history/{{ $profile->token_id }}">View Payment History</a>&nbsp;&nbsp;
                <a href="/add-status/{{$profile->token_id}}">Update Plan</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

var Tawk_API=a4633b19c799868783d9f86d7c669ecae48ff349||{};
Tawk_API.visitor = {
name : 'Test Name',
email : 'visitor@email.com',
hash : 'tesh-value'
};

var Tawk_LoadStart=new Date();
<!-- rest of the tawk.to widget code -->
</script>

</section>
@endsection
