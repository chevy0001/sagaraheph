<form method="post" action="/leads">
  @csrf
    <div class="form-group">
        <label for="name">Your Name*</label>
        <input type="text" class="form-control" id="name" name="name" required>
        <div id="nameInvalidFeedback" class="invalid-feedback">
            Please enter your name.
        </div>
    </div>
    <div class="form-group">
        <label for="phoneNo">Phone No.*</label>
        <input type="phone" class="form-control" id="phoneNo" name="phoneNo" required>
      </div>
    <div class="form-group">
        <label for="emailAddress">Email address*</label>
        <input type="email" class="form-control" id="emailAddress" name="emailAddress" required>
    </div>
   <div class="form-group">
        <label for="emailAddress">Plan</label>
        <select class="form-control" id="plan" name="plan">
            <option value="{{$plan ?? 'REGULAR'}}">
                {{$plan ?? 'Regular'}}
            </option>

            <option value="REGULAR">
                Regular
            </option>
            <option value="ELITE">
                Elite
            </option >
            <option value="VIP">    
                VIP
            </option>
        </select>
    </div>
    <div class="form-group">
        <label for="clientMessage">Message/Questions</label>
        <textarea class="form-control" id="clientMessage" name="clientMessage"></textarea>
    </div>
    <button type="submit" class="primary-btn btn-normal btn mt-4">Submit Quote</button>
</form>