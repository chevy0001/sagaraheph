@extends('layouts.app')
@section('content')
<section class="hero-section">
    <div class="set-description">
        <div class="hero-content">
            <div class="text-center">
                <h1 class="font-weight-semibold text-uppercase mb-4">
                    Latest Blog
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section Begin -->
<section class="team-section mb-10">
    <div class="blog-container">
        <div class="container">
            <div class="row featured-blog mb-4">
                <div class="col-12 col-md-4 px-0 blog-img-container">
                @if($latest)   
                    <img src="{{ asset('/storage/'.$latest->image)}}" class="blog-featured-img">
               @endif 
                </div>
                <div class="col-12 col-md-8">
                    <div class="description">
                        <span class="author">
                            Sa Garahe PH
                        </span>
                        <h1 class="mb-3">
                            <a href="/blog/{{$latest->slug ?? ''}}">{{$latest->title ?? ''}}</a>
                        </h1> 
                        <p class="mb-9">
                            {!! @substr($latest->content, 0,150)!!}...
                        </p>
                        <a href="/blog/{{$latest->slug ?? ''}}">Read More</a>
                    </div>
                </div>
            </div>

          {{-- Start of Row--}}  
            <div class="row">

               @foreach($blogs as $blog ) 
                <div class="col-12 col-md-6 mb-4 pl-md-0">
                    <div class="blogs">
                        <span class="author">
                            Sa Garahe PH
                        </span>
                        <h1 class="mb-3">
                            {{$blog->title}}
                        </h1> 
                        <p class="mb-9">
                           {!!@substr($blog->content, 0,150) !!}...
                        </p>
                        <a href="/blog/{{$blog->slug}}">Read More</a>
                    </div>
                </div>
               @endforeach 
               {{ $blogs->links() }}
                {{-- 
                <div class="col-12 col-md-6 pr-md-0">
                    <div class="blogs">
                        <span class="author">
                            Author Name
                        </span>
                        <h1 class="mb-3">
                            Keys to writing copy that actually converts and sells users
                        </h1> 
                        <p class="mb-9">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore...
                        </p>
                        <a href="#">Read More</a>
                    </div>
                </div>
                --}}
            </div>
        {{-- End of Row--}}

        </div>
    </div>
</section>
<!-- Blog Section End -->
<section class="newsletter spad">
    <div class="container">
        <h3 class="text-center">Subsrcibe to our newsletter</h3>
        <form class="text-center" method="post" action="/subscribe">
            @csrf
            <div class="form-group">
                <input type="email" class="form-control" id="emailAddress" name="emailAddress" placeholder="YOUR EMAIL ADDRESS" required>
            </div>
            <button type="submit" class="primary-btn btn-normal btn mt-4">Subscribe Now</button>
        </form>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f536b8ba9247079"></script>
</section>
@endsection