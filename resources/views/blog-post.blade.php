@extends('layouts.app')
@section('content')
<section class="hero-section mb-5">
    <div class="blogBg" style="background-image: linear-gradient(180deg, rgba(31, 33, 51, 0) 0%, #11121C 100%), url('../storage/{{$blogs->image}}')">

        <div class="hero-content">
            <div class="text-center">
                <h1 class="blog-title font-weight-bold text-uppercase mb-4">
                    {{$blogs->title}}
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section Begin -->
<section class="team-section mb-10">
    <div class="description">
        <div class="text-center author">
            <p>Aug. 07, 2020</p>
            <p>By: Sagaraheph</p>
        </div>
        <div class="row my-5">
            {!!$blogs->content!!}
        </div>
        <div class="text-center">
            <p class="mb-3">Share</p>
            <div class="fa-social">
                <a href="#"><i class="fa fa-facebook"></i></a>&nbsp;&nbsp;
            <a href="#"><i class="fa fa-twitter"></i></a>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section End -->
<section class="newsletter mb-11">
    <div class="container">
        <h3 class="text-center">Subsrcibe to our newsletter</h3>
        <form class="text-center">
            <div class="form-group">
                <input type="email" class="form-control" id="emailAddress" placeholder="YOUR EMAIL ADDRESS" required>
            </div>
            <button type="submit" class="primary-btn btn-normal btn mt-4">Subscribe Now</button>
        </form>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f536b8ba9247079"></script>
</section>
@endsection