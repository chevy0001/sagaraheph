@extends('layouts.app')
@section('content')
<section class="hero-section">
    <div class="set-description">
        <div class="left-hero-content">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h1 class="services-cover-title font-weight-bold text-uppercase mb-0">
                            Garahe that is amazing
                        </h1>
                    </div>
                    <div class="col-12 col-md-6 services-cover-desc">
                        <h5 class="font-weight-lighter page-description text-uppercase mb-0">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="container mb-11">
    <img src="{{ asset('images/services-a.jpg') }}" class="media-container">
</section>
<!-- Services Section Begin -->
<section class="classes-section border-0 mb-11" id="services">
    <div class="container">
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="section-title">
                    <h1 class="mb-3">Elite Garage in Davao</h1>
                    <span class="sub-description">
                        If you don’t try this service, you won’t become the superhero you were meant to be
                    </span>
                </div>
            </div>
        </div>
        <div class="row services">
            <div class="col-12 col-lg-6 px-0 image img-container mb-4">
                <img src="{{ asset('images/elite-garage.png') }}">
            </div>
            <div class="col-12 col-lg-6 description">
                <div class="d-flex mb-5">
                    <img src="{{ asset('images/wash&fix.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Wash and Fix</h6>
                        <p class="mt-0 mb-0">
                            Experience modern carwash technology.
                            Spare parts and tools are available for your convenience.
                        </p>
                    </div>
                </div>
                <div class="d-flex mb-5">
                    <img src="{{ asset('images/network.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Network with Peers</h6>
                        <p class="mt-0 mb-0">
                            Discover people with the same mindset and interest over coffee.
                        </p>
                    </div>
                </div>
                <div class="d-flex mb-5">
                    <img src="{{ asset('images/meditate.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Chill and Meditate</h6>
                        <p class="mt-0 mb-0">
                            Your own exclusive VIP room is waiting for you to empty your
                            mind from stress and be with yourself.
                        </p>
                    </div>
                </div>
                <div class="d-flex">
                    <img src="{{ asset('images/business.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Business</h6>
                        <p class="mt-0 mb-0">
                            Board rooms are available for Meetings, Discussions and Conference.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->
<!-- Banner Section Begin -->
<section class="banner-section">
    <div class="banner-bg">
        <div class="row">
            <div class="col-12 col-md-6 flex-grow-1 mb-4">
                <h1>Ready to get started?</h1>
                <p>Get a free quote or talk to us to get more deals.</p>
            </div>
            <div class="col-12 col-md-6 banner-buttons">
                <a href="/get-quote" class="primary-btn btn-normal mb-3 mr-lg-1" role="button">Get a quote</a>
                <a href="/contact" class="secondary-btn btn-normal mb-3 ml-lg-1" role="button">Contact us</a>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->
<!-- Free Quote Section Begin -->
<section class="team-section spad">
    <div class="blog-container">
        <div class="container">
            <div class="row services">
                <div class="col-12 col-lg-6 mb-4">
                    <h1>Get a free quote</h1>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore...
                    </p>
                    <p class="mb-0">Follow us</p>
                    <div class="fa-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-12 col-lg-6 px-0 free-quote-form">
                    <form>
                        <div class="form-group">
                            <label for="name">Your Name*</label>
                            <input type="text" class="form-control" id="name" required>
                            <div id="nameInvalidFeedback" class="invalid-feedback">
                                Please enter your name.
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phoneNo">Phone No.*</label>
                            <input type="phone" class="form-control" id="phoneNo" required>
                          </div>
                        <div class="form-group">
                            <label for="emailAddress">Email address*</label>
                            <input type="email" class="form-control" id="emailAddress" required>
                        </div>
                        <div class="form-group">
                            <label for="clientMessage">Message/Questions</label>
                            <textarea class="form-control" id="clientMessage"></textarea>
                        </div>
                        <button type="submit" class="primary-btn btn-normal btn mt-4">Submit Quote</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Free Quote Section End -->
@endsection