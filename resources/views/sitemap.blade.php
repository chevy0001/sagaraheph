<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<!-- created with Free Online Sitemap Generator www.xml-sitemaps.com -->


<url>
  <loc>https://sagarahe.com/</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>1.00</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/get-quote</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.80</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/services</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.80</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/pricing</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.80</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/about</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.80</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/blog</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.80</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/contact</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.80</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/login</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.80</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/public/blog</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.64</priority>
  <changefreq>weekly</changefreq>
</url>
<url>
  <loc>https://sagarahe.com/password/reset</loc>
  <lastmod>2020-09-02T09:27:05+00:00</lastmod>
  <priority>0.64</priority>
  <changefreq>weekly</changefreq>
</url>


  @foreach($blogs as $blog)
    <url>
      <loc>https://sagarahe.com/{{$blog->slug}}</loc>
      <lastmod>{{str_replace(" ","T",$blog->created_at)}}</lastmod>
      <priority>0.80</priority>
      <changefreq>weekly</changefreq>
    </url>
  @endforeach


</urlset>