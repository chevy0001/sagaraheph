@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.admin-sidebar')
            </div>
            <div class="col-12 col-md-9 bg-white ">
                <div class="container-fluid p-md-5 pt-5 pb-5 mb-9">
                    <div class="row">
                        <h3 class="flex-grow-1 justify-content-center">Enquiries</h3>
                    </div>
                    <div class="row mt-4">
                        <div class="card">
                            <div class="card-header">
                                All Enquiries
                            </div>
                            <div class="card-body">
                                <table class="table table-hover user-list">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Client Name</th>
                                            <th scope="col">Phone</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Message</th>
                                            <th scope="col">Plan</th>
                                            <th scope="col">Date</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($enq as $enquiries) 
                                        <tr>
                                            <td>{{$enquiries->id}}</td>
                                            <td>{{$enquiries->name}}</td>
                                            <td>{{$enquiries->phone_number}}</td>
                                            <td>{{$enquiries->email}}</td>
                                            <td>{{$enquiries->message}}</td>
                                            <td>{{$enquiries->plan}}</td>
                                            <td>{{$enquiries->created_at}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{$enq->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
