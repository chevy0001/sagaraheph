@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @if(auth()->user()->role === '1')
                    @include('partials.admin-sidebar')
                @else
                    @include('partials.user-sidebar')
                @endif    
            </div>
            <div class="col-12 col-md-9 bg-white ">
                <div class="container-fluid p-md-5 pt-5 pb-5">
                   
                 @if(auth()->user()->role === '1')
                  <div class="row mb-3">
                      <h3 class="flex-grow-1 justify-content-center">Invoice</h3>
                      <a href="" role="button" class="secondary-btn btn-normal">Create Invoice</a>
                  </div>
                 @endif
                  <div class="row">
                    <div class="card">
                      <div class="card-header">
                          Invoice #{{$invoice->invoice_number}}
                    </div>
                    <div class="card-body">
                      <div class="row">
                          <div class="col-12 col-md-8 mb-4">
                              <h4 class="mb-1">{{$user->firstname}} {{$user->lastname}}</h4>
                              <h6 class="mb-2">{{$user->street ?? ''}}, {{$user->barangay ?? ''}},{{$user->city ?? 'Davao City'}},{{$user->region??'Davao del Sur'}},{{$user->country ?? 'Philippines'}}</h6>
                              <h6>{{$user->phone_number}}</h6>
                            </div>
                            <div class="col-12 col-md-4 mb-4">
                                <h4 class="mb-1">Sagaraheph</h4>
                                <h6 class="mb-2">Obrero, Davao City</h6>
                                <h6>(082) 236- 1234</h6>
                            </div>
                      </div>
                       {{-- <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Receipt No.</th>
                                    <th scope="col">Plan Name</th>
                                    <th scope="col">Plan Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($paymentHistory ?? '')
                                @foreach($paymentHistory as $history)
                                <tr>
                                    <th scope="row">{{$history->id ?? ''}}</th>
                                    <td>{{$history->receipt_no ?? ''}}</td>
                                    <td>VIP(Test)</td>
                                    <td>{{$history->amount ?? ''}}</td>
                                    <td>
                                        <a href="{{asset('storage/'.$history->receipt) ?? ''}}" target="_blank">  <img class="w-25 img-fluid "  src="{{asset('storage/'.$history->receipt) ?? ''}}"></a></td>
                                        <td>{{$history->created_at ?? ''}}</td>
                                        <td>
                                        @if($history->is_verified === 0)
                                            No
                                        @else
                                            Yes
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        --}}
                  <hr/>
                  <div class="row">
                      <div class="col-12 col-md-8 mb-4">
                          <h6 class="mb-2">Please pay your invoice by your due date, thank you.</h6>
                      </div>
                      <div class="col-12 col-md-4 mb-4">
                          <span>{{$invoice->status_name}} Membership Fee:</span> <span class="pull-right">{{$invoice->total/$invoice->seats}}</span><br/>
                          <span>Number of Seats:</span> <span class="pull-right">{{$invoice->seats}}</span><br/>
                          <span>Sub-total:</span> <span class="pull-right">{{$invoice->total}}</span><br/>
                          <span>Tax:</span> <span class="pull-right">0</span><br/>
                          <span>Total:</span> <span class="pull-right">{{$invoice->total}}</span><br/><br/>
                          <strong><span>Amount Due:</span> <span class="pull-right">{{$invoice->total}}</span></strong>
                         
                           <br/><br/>
                      @if(auth()->user()->role === '1')     
                          <span class="pull-right"> <a href="/user-profile/{{$invoice->user_token_id}}" class="primary-btn btn-normal "> 
                    {{ __('Confirm') }}
                  @else
                  <span class="pull-right"> <a href="/add-payment/{{auth()->user()->token_id}}/{{$invoice->invoice_number}}" class="primary-btn btn-normal "> 
                    {{ __('Pay Invoice') }}
                  @endif  
                
                </a></span>
              </div>


                            
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection


