@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.admin-sidebar')
            </div>
            <div class="col-12 col-md-9 bg-white ">
                <div class="container-fluid p-md-5 pt-5 pb-5 mb-9">
                    <div class="row">
                        <h3>Dashboard</h3>
                    </div>
                    <div class="row">
                        <div class="user-img d-flex align-item-center justify-content-center">
                            <div class="profile-image">

                                @if(@isset($profile))
                                <img src="{{ asset('storage/'.$profile->profile_photo) }}" class="w-100" style="border-radius: 50%;">
                                @else
                                <img src="{{ asset('images/default-img.svg') }}">
                                @endif
                            </div>
                        </div>
                        <table class="table mt-5">
                            <tbody>
                                <tr>
                                    <td class="label">Full Name</td>
                                    <td class="data">{{$profile->firstname ?? 'Your First Name'}} {{$profile->lastname ?? 'Your Last Name'}}</td>
                                </tr>
                                <tr>
                                    <td class="label">Email</td>
                                    <td class="data">{{$profile->email}}</td>
                                </tr>
                                <tr>
                                    <td class="label">Phone No.</td>
                                    <td class="data">{{$profile->phone_number}}</td>
                                </tr>
                                <tr>
                                    <td class="label">Status</td>
                                    <td class="data">
                                        @if($status)
                                            {{$status->status_name ?? ''}}
                                        @else
                                            <a href="/add-status/{{$profile->token_id}}">Add Status</a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label">Actions</td>
                                    <td class="data">
                                        <a href="/edit/{{$profile->token_id}}">Edit Profile</a> 
                                        | <a href="/add-status/{{$profile->token_id}}">Update Plan</a> | <a href="/delete/{{$profile->token_id}}">Delete</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-5 mb-3">
                        <h3>Inclusive Benefits</h3>
                    </div>
                    <div class="row">
                        <div class="card">
                            <div class="card-header">
                                @if($status)
                                    {{$status->status_name ?? 'Pending'}} Membership
                                @else
                                    <a href="/add-status/{{$profile->token_id}}">Add a plan</a>
                                @endif 
                            </div>
                            <div class="card-body">
                                <p class="card-title">Payment History</p>
                                <table class="table table-hover user-list">
                                    <thead>
                                        <tr>
                                            <th scope="col">Payment ID</th>
                                            <th scope="col">Receipt No.</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Receipt Image</th>
                                            <th scope="col">Date Uploaded</th>
                                            <th scope="col">Verified</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($paymentHistory)
                                        @foreach($paymentHistory as $history)
                                        <tr>
                                            <td>{{$history->id ?? ''}}</td>
                                            <td>{{$history->receipt_no ?? ''}}</td>
                                            <td>{{$history->status_type}}</td>
                                            <td>{{$history->amount ?? ''}}</td>
                                            <td><a href="{{asset('storage/'.$history->receipt) ?? ''}}" target="_blank">  <img class="w-25 img-fluid "  src="{{asset('storage/'.$history->receipt) ?? ''}}"></a></td>
                                            <td>{{$history->created_at ?? ''}}</td>
                                            <td>
                                                @if($history->is_verified == 0)
                                                <form method="post" action="/update-payment/{{$history->id}}">
                                                    @csrf
                                                    @method('PATCH')
                                                    <input type="hidden" name="verify" id="verify" value="1">
                                                    <input class="btn btn-primary" type="submit" name="submit" value="Verify">
                                                </form>
                                                @else
                                                    Yes
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
