@extends('layouts.admin')

@section('content')
<section class="client-spad">

    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @if(auth()->user()->role === '1')
                    @include('partials.admin-sidebar')
                @else
                    @include('partials.user-sidebar')
                @endif 
            </div>
            <div class="col-12 col-md-9 bg-white ">
              <div class="container-fluid p-md-5 pt-5 pb-5">
                  
               @if(Auth::user()->role === '1') 
                <div class="row mb-3">
                    <h3 class="flex-grow-1 justify-content-center">Invoice</h3>
                    <a href="" role="button" class="secondary-btn btn-normal">Create Invoice</a>
                </div>
               @endif 
                <div class="row">
                  <div class="card">
                    <div class="card-header">
                        All Invoice
                    </div>
                    <div class="card-body">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">Invoice Number</th>
                              <th scope="col">Client Name</th>
                              <th scope="col">Status</th>
                              <th scope="col">Amount</th>
                              <th scope="col">Receipt Image</th>
                              <th scope="col">Date Uploaded</th>
                              <th scope="col">Verified</th>
                            </tr>
                          </thead>
                          <tbody>
                            @if($invoices ?? '')
                            @foreach($invoices as $invoice)
        <tr>
          <th scope="row">
            <a href="/invoice-show/{{$invoice->user_token_id}}/{{$invoice->invoice_number}}">{{$invoice->invoice_number ?? ''}}
            </a></th>
          <td>{{$invoice->firstname ?? auth()->user()->firstname}} {{$invoice->lastname ?? auth()->user()->lastname}}</td>
          <td>{{$invoice->status_name}}</td>
          <td>{{$invoice->total ?? ''}}</td>
          {{--<td>
          <a href="{{asset('storage/'.$history->receipt) ?? ''}}" target="_blank">  <img class="w-25 img-fluid "  src="{{asset('storage/'.$history->receipt) ?? ''}}"></a></td>--}}
                            <td>
                              @if($invoice->receipt)
                              <a href="{{asset('storage/'.$invoice->receipt) ?? '/'}}" target="_blank">  <img class="w-25 img-fluid "  src="{{asset('storage/'.$invoice->receipt) ?? ''}}"></a>
                              @else
                              No Reciept
                              @endif
                            </td>
                            
                              <td>{{$invoice->updated_at ?? ''}}</td>
                              <td>
                                  @if($invoice->is_verified === 0)
                                      <a href='/user-profile/{{$invoice->client_token_id}}'>Verify</a>
                                  @elseif($invoice->is_verified == NULL)
                                      <a href='/user-profile/{{$invoice->client_token_id}}'>Verify</a>
                                  @else
                                      Yes
                                  @endif
                              </td>
                            </tr>
                            @endforeach
                            @endif 
                          </tbody>
                        </table>
                      </div>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
@endsection


