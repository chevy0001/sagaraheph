@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.admin-sidebar')
            </div>
            <div class="col-12 col-md-9 bg-white ">
              <div class="p-md-5 pt-5 pb-5">
                <div class="row container-fluid">
                  <h3 class="justify-content-center">Create New Blog</h3>
                  <div class="user-form">
                    <form method="post" action="/store-blog" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="title" class="col-form-label">Title</label>
                        <div>
                          <input type="text" name="title" id="title" class="form-control" required>
                        </div>
                      </div>   
                      
                      <div class="form-group">
                        <label for="image" class="col-form-label">Header Image</label>
                        <div>
                          <input type="file" name="image" id="image" class="form-control" required>
                        </div>
                      </div>   
                      <div class="form-group">
                        <label for="content" class="col-form-label">Content</label>
                        <div>
                          <textarea id="content" name="content" class="form-control" ></textarea>
                        </div>
                      </div>
                      <div class="form-group mt-5">
                        <button type="submit" class="primary-btn btn-normal">
                          {{ __('Submit') }}
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</section>
<script src="https://cdn.tiny.cloud/1/6tq2190ie8f0lsyfwdismrpy0unow8hi0cf5tg0ia8wehlam/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
    selector: '#content'
    });
</script>
@endsection