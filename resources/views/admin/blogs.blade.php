@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.admin-sidebar')
            </div>
            <div class="col-12 col-md-9 bg-white ">
                <div class="container-fluid p-md-5 pt-5 pb-5 mb-9">
                    <div class="row">
                        <h3 class="flex-grow-1 justify-content-center">Blog List</h3>
                        <a href="/add-blog" role="button" class="secondary-btn btn-normal">Create new blog</a>
                    </div>
                    <div class="row mt-4">
                        <div class="card">
                            <div class="card-header">
                                All Blogs
                            </div>
                            <div class="card-body">
                                <table class="table table-hover user-list">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Title</th>
                                            <th scope="col">Date Posted</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($blogs as $blog)
                                        <tr>
                                            <td>{{$blog->id ?? ''}}</td>
                                            <td><a href="/blog/{{$blog->slug}}">{{$blog->title ?? ''}}</a></td>
                                            <td>{{$blog->created_at ?? ''}}</td>
                                            <td><a href="/edit-blog/{{$blog->id}}"> Edit</a>  |Delete</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
