@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.admin-sidebar')
            </div>
            <div class="col-12 col-md-9 bg-white ">
                <div class="container-fluid p-md-5 pt-5 pb-5 mb-9">
                    <div class="row">
                        <h3 class="flex-grow-1 justify-content-center">Dashboard</h3>
                        <a href="/register-user" role="button" class="secondary-btn btn-normal">Register User</a>
                    </div>
                    <div class="row mt-4">
                        <div class="card">
                            <div class="card-header">
                                Users
                            </div>
                            <div class="card-body">
                                <table class="table table-hover user-list">
                                    <thead>
                                        <tr>
                                            <th scope="col">ID</th>
                                            <th scope="col">Fullname</th>
                                            <th scope="col">Phone</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                        <tr>
                                            <td>{{$user->id ?? ''}}</td>
                                            <td><a href="/user-profile/{{$user->token_id}}">{{$user->firstname ?? ''}} {{$user->lastname ?? ''}}</a></td>
                                            <td>{{$user->phone_number ?? ''}}</td>
                                            <td>action</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
