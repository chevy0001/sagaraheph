@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.admin-sidebar')
            </div>
            <div class="col-12 col-md-9 bg-white ">
                <div class="container-fluid p-md-5 pt-5 pb-5 mb-9">
                    <div class="row">
                        <h3 class="flex-grow-1 justify-content-center">Enquiries</h3>
                    </div>
                    <div class="row mt-4">
                        <div class="card">
                            <div class="card-header">
                                All Enquiries
                            </div>
                            <div class="card-body">
                                <h4 class="mb-3">Enquiry No. 1</h4>
                                <h6 class="mb-1">From: Joseph Kyo</h6>
                                <h6 class="mb-1">Email: joseph.kyo1989@gmail.com</h6>
                                <h6 class="mb-1">Phone: 0911 123 4567</h6>
                                <h6 class="mb-1">Message:</h6>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna
                                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                    Duis aute irure dolor in reprehenderit in voluptate velit
                                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                                    occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <button class="primary-btn btn-normal">Reply</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
