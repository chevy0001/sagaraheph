@extends('layouts.client')

@section('content')
<section class="client-spad">
  <div class="container-fluid mt-9 mb-9">
    <div class="row">
        <div class="col-12 col-md-3">
            @include('partials.user-sidebar')
        </div>
        <div class="col-12 col-md-9 bg-white ">
          <div class="container-fluid p-md-5 pt-5 pb-5">
            <div class="row mb-3">
                <h3 class="flex-grow-1 justify-content-center">Add a payment</h3>
            </div>
            <div class="row">
              <div class="card user-form">
                <div class="card-header">
                    Membership
                </div>
                <div class="card-body col-md-8 mx-auto">
                  <form method="post" action="/store-reciept/{{$tokenid}}" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                          <label for="status" class="col-form-label">Select a Plan</label>
                          <div class="form-group">
                            <select id="status" name="status" type="text" class="form-control">
                              <option value="{{$invoice->status_name ?? 'Regular'}}">{{$invoice->status_name ?? 'Regular - 15K/Seat'}} - {{($invoice->total/$invoice->seats)/1000}}K/Seat</option>
                              <option value="Regular">Regular - 15K/Seat</option>
                                <option value="VIP">VIP - 45K/Seat</option>
                                <option value="Elite">Elite - 60K/Seat</option>
                            </select>
                          </div>
                      </div>
                    <div class="form-group">
                      <label for="amount" class="col-form-label">Amount</label>
                      <div>
                          <input id="amount" type="number" class="form-control @error('amount') is-invalid @enderror" name="amount" value="{{ $invoice->total ?? old('amount') }}" required autocomplete="amount"  placeholder="1000" autofocus>
                          @error('Amount')
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                             </span>
                          <input id="invoice_number" type="hidden" name="invoice_number" value="{{ $invoice->invoice_number }}" required autocomplete="amount"  placeholder="" autofocus>
                         @enderror
                      </div>
                      <div>
                          <input id="invoice_number" type="hidden" name="invoice_number" value="{{ $invoice->invoice_number }}" required autocomplete="amount"  placeholder="" autofocus>
                          @error('Invoice')
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                             </span>
                          
                         @enderror
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="amount" class="col-form-label">OR Number</label>
                      <div>
                          <input id="or_number" type="text" class="form-control @error('or_number') is-invalid @enderror" name="or_number" value="{{ old('or_number') }}" required autocomplete="or_number"  autofocus>
                          @error('OR_Number')
                             <span class="invalid-feedback" role="alert">
                                 <strong>{{ $message }}</strong>
                             </span>
                          @enderror
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="Payment Reciept" class="col-form-label">Upload Receipt</label>
                      <div>
                          <input id="reciept" type="file" class="form-control @error('reciept') is-invalid @enderror" name="reciept" value="{{ old('reciept') }}" required autocomplete="reciept" autofocus>
                          @error('reciept')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                      </div>
                    </div>
                    <div class="form-group mt-5">
                      <div class="text-center">
                          <button type="submit" class="primary-btn btn-normal">
                              {{ __('Upload Reciept') }}
                          </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</section>
@endsection


