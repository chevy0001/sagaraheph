@extends('layouts.client')

@section('content')
<section class="client-spad">

    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">
                @include('partials.user-sidebar')
            </div>
            <div class="col-12 col-md-9 bg-white ">
                <div class="container-fluid p-md-5 pt-5 pb-5">
                    <div class="row mb-3">
                        <h3 class="flex-grow-1 justify-content-center">Payments</h3>
                        {{--<a href="/add-payment/{{$tokenid}}" role="button" class="secondary-btn btn-normal">Add Payment</a>--}}
                    </div>
                    <div class="row">
                      <div class="card">
                        <div class="card-header">

                          @if($status)
                           
                            @if($status->is_verified==1)
                          {{$status->status_name ?? 'Not Registered Yet'}} Membership
                            @else
                           {{$status->status_name ?? 'Not Registered Yet'}} - Pending Request
                            @endif
                          @endif
                        </div>
                          <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Payment ID</th>
                                            <th scope="col">Receipt No.</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Amount</th>
                                            <th scope="col">Receipt Image</th>
                                            <th scope="col">Date Uploaded</th>
                                            <th scope="col">Verified</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($paymentHistory ?? '')
                                        @foreach($paymentHistory as $history)
                                        <tr>
                                            <th scope="row">{{$history->id ?? ''}}</th>
                                            <td>{{$history->receipt_no ?? ''}}</td>
                                            <td>{{$history->status_type}}</td>
                                            <td>{{$history->amount ?? ''}}</td>
                                            <td>
                                            <a href="{{asset('storage/'.$history->receipt) ?? ''}}" target="_blank">  <img class="w-25 img-fluid "  src="{{asset('storage/'.$history->receipt) ?? ''}}"></a></td>
                                            <td>{{$history->created_at ?? ''}}</td>
                                            <td>
                                                @if($history->is_verified == 0)
                                                    No
                                                @else
                                                    Yes
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
@endsection


