@extends('layouts.app')
@section('content')
<section class="hero-section">
    <div class="set-description">
        <div class="left-hero-content">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 services-cover-gap">
                        <h1 class="services-cover-title font-weight-bold text-uppercase mb-0">
                            About Sa Garahe
                        </h1>
                    </div>
                    <div class="col-12 col-md-6 services-cover-desc">
                        <h5 class="font-weight-lighter page-description text-uppercase mb-0">
                            Discover Sa Garahe's amazing community and become someone you meant to be.
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section Begin -->
<section class="classes-section border-0 mb-11" id="services">
    <div class="container">
        <div class="row services">
            <div class="col-12 col-md-6 description">
                <p>
                    
                </p>
<p>

<strong>Sa Garahe</strong> is founded by the head car wash boy <a style="color: #ffc107" target='_blank' href="https://www.facebook.com/romanfinancialplanner"><strong>Mr. Roman Linejan</strong></a>, he also founded <a target='_blank'href="https://www.facebook.com/StartUpzCoWorkingSpace/" style="color: #ffc107">StartUpz Co-Working Space</a>, Financial Consultant at <a href="http://fthmovement.com.ph" target="_blank" style="color: #ffc107"> FTH Business Consultancy Services</a> and Senior Unit Head at <a target="_blank" href="https://www.facebook.com/AXA.Philippines/" style="color: #ffc107">AXA Philippines</a>.
<br/><br/>
Sa Garahe PH main purpose is to provide place for men. Not an ordinary place for extra-ordinary men. It is exclusive and limited. Sa Garahe is a place where we can relax from stressful workplace and workdays, a place where men can escape reality for a while, can have peace of mind, a place where we can be ourselves, a place where we can get dirty and a place where we can have our time.

<br/><br/>
Sa Garahe PH provides services such as carwash, tools, equipments and spare parts, car painting and detailing, coffee shop, barber shop, board rooms. We also offer Limited and Exclsuive VIP Rooms, Elite Rooms, and Regular seats.

<br/><br/>
Imagine what you can do in your own place, Sa Garahe is a brilliant idea especially in this time of pandemic. Yes! this place is designed for the <strong style="color: #ffc107">New Normal.</strong> Exclusivity and Safety is our main goal. 
</p>
            </div>
            <div class="col-12 col-md-6 px-0 image img-container">
                <img src="{{ asset('images/services-a.jpg') }}">
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->
<!-- Services Section Begin -->
<section class="classes-section border-0 mb-11" id="services">
    <div class="container">
        <div class="row mb-3">
            <div class="col-lg-12">
                <div class="section-title">
                    <h1 class="mb-3">Elite Garage in Davao</h1>
                    <span class="sub-description">
                        If you don’t try this service, you will miss something extraordinary
                    </span>
                </div>
            </div>
        </div>
        <div class="row services">
            <div class="col-12 col-lg-6 px-0 image img-container mb-4">
                <img src="{{ asset('images/elite-garage.png') }}">
            </div>
            <div class="col-12 col-lg-6 description">
                <div class="d-flex mb-5">
                    <img src="{{ asset('images/wash&fix.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Wash and Fix</h6>
                        <p class="mt-0 mb-0">
                            Experience convenient carwash services.
                            Spare parts, tools and best crew are available for your convenience.
                        </p>
                    </div>
                </div>
                <div class="d-flex mb-5">
                    <img src="{{ asset('images/network.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Network with Peers</h6>
                        <p class="mt-0 mb-0">
                            Discover people with the same mindset and interest over coffee.
                        </p>
                    </div>
                </div>
                <div class="d-flex mb-5">
                    <img src="{{ asset('images/meditate.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Chill and Meditate</h6>
                        <p class="mt-0 mb-0">
                            Your own exclusive VIP room is waiting for you to empty your mind from stress and plan for your next move.
                        </p>
                    </div>
                </div>
                <div class="d-flex">
                    <img src="{{ asset('images/business.png') }}" width="109px" height="109px">
                    <div class="ml-3 justify-content-center">
                        <h6>Business</h6>
                        <p class="mt-0 mb-0">
                            Board rooms are available for Meetings, Discussions and Conference.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->
<!-- Banner Section Begin -->
<section class="banner-section">
    <div class="banner-bg">
        <div class="row">
            <div class="col-12 col-md-6 flex-grow-1 mb-4">
                <h1>Ready to get started?</h1>
                <p>Get a free quote or talk to us to get more deals.</p>
            </div>
            <div class="col-12 col-md-6 banner-buttons">
                <a href="/get-quote" class="primary-btn btn-normal mb-3 mr-lg-1" role="button">Get a quote</a>
                <a href="/contact" class="secondary-btn btn-normal mb-3 ml-lg-1" role="button">Contact us</a>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->
<!-- Blog Section End -->
<section class="newsletter spad">
    <div class="container">
        <h3 class="text-center">Subsrcibe to our newsletter</h3>
        <form class="text-center">
            <div class="form-group">
                <input type="email" class="form-control" id="emailAddress" placeholder="YOUR EMAIL ADDRESS" required>
            </div>
            <button type="submit" class="primary-btn btn-normal btn mt-4">Subscribe Now</button>
        </form>
    </div>
    {{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f536b8ba9247079"></script>--}}
</section>
@endsection