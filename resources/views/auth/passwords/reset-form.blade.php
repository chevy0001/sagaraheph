@extends('layouts.app')

@section('content')
<section class="spad">
    <div class="container mt-9 mb-9 sign-in">
        <div class="text-center mb-5">
            <h1 class="font-weight-semibold text-uppercase mb-4">
                Reset Password
            </h1>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif

                    <form method="POST" action="/password/resetpass">
                        @csrf
                        @method('PATCH')
                      <div class="container">
                        <div class="form-group">
                          <label for="password" class="col-form-label">{{ __('Password') }}</label>
                          <div>
                            <input id="password" type="password" class="form-control text-white @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="password" autofocus>

                            @error('password')
                              <span class="invalid-feedback text-white" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                            @enderror
                        </div>
                      </div>

                      <div class="form-group">
                          
                          <div>
                            <input id="token" type="hidden" class="form-control text-white @error('token') is-invalid @enderror" name="token"  required value="{{$token}}" >

                        </div>
                      </div>

                        <div class="form-group">
                          <label for="confirmPassword" class="col-form-label">{{ __('Confirm Password') }}</label>
                          <div>
                            <input id="confirmPassword" type="password" class="form-control text-white @error('confirmPassword') is-invalid @enderror" name="password_confirmation" required autocomplete="confirmPassword">

                            @error('confirmPassword')
                              <span class="invalid-feedback text-white" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                            @enderror
                          </div>
                        </div>

                        <div class="form-group mt-5">
                            <div class="text-center">
                              <button type="submit"  class="primary-btn btn-normal">
                                  {{ __('Reset Password') }}
                              </button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>
@endsection
