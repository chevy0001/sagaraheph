@extends('layouts.app')

@section('content')
<section class="spad">
    <div class="container mt-9 mb-9 sign-in">
        <div class="text-center mb-5">
            <h1 class="font-weight-semibold text-uppercase mb-4">
                Sign in to your account
            </h1>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="container">
                                <div class="form-group">
                                    <label for="email" class="col-form-label">{{ __('Email Address') }}</label>
                                    <div>
                                        <input id="email" type="email" class="form-control text-white @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
    
                                        @error('email')
                                            <span class="invalid-feedback text-white" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
    
                                <div class="form-group">
                                    <label for="password" class="col-form-label">{{ __('Password') }}</label>
                                    <div>
                                        <input id="password" type="password" class="form-control text-white @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
    
                                        @error('password')
                                            <span class="invalid-feedback text-white" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
    
                                <div class="form-group">
                                    <div>
                                        @if (Route::has('password.request'))
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                {{ __('Forgot Your Password?') }}
                                            </a>
                                        @endif
                                    </div>
                                </div>
    
                                <div class="form-group mt-5">
                                    <div class="text-center">
                                        <button type="submit" class="primary-btn btn-normal">
                                            {{ __('Sign in') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center mt-5">
            <a href="/contact" class="font-weight-semibold text-uppercase mb-4">
                Contact us to register
            </a>
        </div>
    </div>
</section>
@endsection
