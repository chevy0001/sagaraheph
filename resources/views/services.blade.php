@extends('layouts.app')
@section('content')
<section class="hero-section">
    <div class="set-description">
        <div class="left-hero-content">
            <div class="container">
                <h1 class="page-title font-weight-bold text-uppercase mb-4">
                    Services we offer
                </h1>
                <h3 class="font-weight-lighter page-description text-uppercase">
                    {{--Lorem ipsum dolor sit amet, consectetur adipiscing--}}
                </h3>
                <a href="#services">
                    <img src="{{ asset('images/arrow-down.png') }}">
                </a>
            </div>
        </div>
    </div>
</section>
<!-- Services Section Begin -->
<section class="classes-section border-0 mb-11" id="services">
    <div class="container-fluid">
        <div class="row services">
            <div class="col-12 col-md-6 px-0 image img-container">
                <img src="{{ asset('images/services-a.jpg') }}" class="featured-img">
            </div>
            <div class="col-12 col-md-6 description">
                <h1>Amazing Garahe Setup</h1>
                <p>
                  Aside from quality carwash service Sa Garahe have complete set of spare parts, tools, car painting and detailing services and most importantly best staff that will cater to your needs.

                    {{--Inside your own garahe you can do whatever you want to your car,
                    we have complete set of tools, spare parts are conveniently available and best people who can assist and give what you need.--}}
                </p>
                <a href="/get-quote" class="primary-btn btn-normal mb-md-5 mb-2">Get Quote</a>&nbsp;
                {{--<a href="/services/1" class="secondary-btn btn-normal mb-5">learn More</a>--}}
            </div>
        </div>
        <div class="row services">
            <div class="col-12 col-md-6 description">
                <h1>Relax and Meditate</h1>
                <p>

                  Sa Garahe is designed to provide a relaxation setup not just for your car but a place that you can concentrate and plan for your next move.
                    {{--Whenever you need space from work your VIP room is waiting for
                    you to relieve stress and to condition your mind, body and soul.
                    Being with yourself is a great advantage to know your self more
                    and you will realize its benefits.--}}
                </p>
                <a href="/get-quote" class="primary-btn btn-normal mb-md-5 mb-2">Get Quote</a>&nbsp;
               {{--  <a href="/services/1" class="secondary-btn btn-normal mb-5">learn More</a>--}}
            </div>
            <div class="col-12 col-md-6 px-0 image img-container">
                <img src="{{ asset('images/services-b.jpg') }}" class="featured-img">
            </div>
        </div>
        <div class="row services">
            <div class="col-12 col-md-6 px-0 image img-container">
                <img src="{{ asset('images/services-c.jpg') }}" class="featured-img">
            </div>
            <div class="col-12 col-md-6 description">
                <h1>Chill and Meet</h1>
                <p>
                    Meet great people while having coffee, take advantage of this 
                    rare opportunity since members are no ordinary customers but 
                    with great minds and full of ambitions towards success. Exclusive board rooms are always ready for meetings.
                </p>
                <a href="/get-quote" class="primary-btn btn-normal mb-md-5 mb-2">Get Quote</a>&nbsp;
                {{--<a href="/services/1" class="secondary-btn btn-normal mb-5">learn More</a>--}}
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->
<!-- Banner Section Begin -->
<section class="banner-section-full">
    <div class="banner-bg">
        <div class="text-center">
            <h1>Experience our garage</h1>
            <p class="mb-5">{{--Lorem ipsum dolor sit amet, consectetur adipiscing--}}</p>
            <a href="">
                <img src="{{ asset('images/play-video.png') }}">
            </a>
        </div>
    </div>
</section>
<!-- Banner Section End -->
<!-- Pricing Section Begin -->
<section class="pricing-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <span class="title-description">Our Plans</span>
                    <h1 class="mt-2">Choose your plan</h1>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Regular</h3>
                    <div class="pi-price">
                        <h2>₱15,000</h2>
                        <span>3 Seats Available</span><br/><br/>
                        <span>6 Tables</span><br/>
                        <span>(5 Seats/Table)</span>
                    </div>
                    <ul>
                        <li>5 Day Pass a week to your allocated</li>
                        <li>Seat and Table</li>
                        <li>Car wash and clean</li>
                        <li>Reshuffle every 3 weeks</li>
                    </ul>
                    <a href="/get-quote" class="secondary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Elite</h3>
                    <div class="pi-price">
                        <h2>₱60,000</h2>
                        <span>2 Rooms Remaining</span><br/><br/>
                        <span>3 Rooms + 1 Elite Cube</span>
                    </div>
                    <ul>
                        <li>Room Butler Service</li>
                        <li>25% Discount to all orders</li>
                        <li>5 days pass for Elite Rooms and 2 days</li>
                        <li>Pass for Elite Cubicles a week</li>
                        <li>Car wash and clean</li>
                    </ul>
                    <a href="/get-quote" class="primary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Vip</h3>
                    <div class="pi-price">
                        <h2>₱45,000</h2>
                        <span>5 Rooms Remaining</span><br/><br/>
                        <span>6 Rooms + 3 vip Cubes</span>
                    </div>
                    <ul>
                        <li>Car wash and clean</li>
                        <li>5 days pass for VIP Rooms and 2 days</li>
                        <li>Pass for VIP Cubicles a week</li>
                        <li>Car wash and clean</li>
                    </ul>
                    <a href="/get-quote" class="secondary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Pricing Section End -->
@include('faq');
@endsection
