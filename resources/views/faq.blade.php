<section class="pricing-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h1 class="mt-2">FAQs</h1>
                </div>
            </div>
        </div>
        <div class="accordion" id="sgFAQ">
            <div class="card">
                <div class="card-header" id="faqOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Availability of spare parts (in stock or to be ordered?). If ordered, how many days should I wait?
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="faqOne" data-parent="#sgFAQ">
                    <div class="card-body">
                        The spare parts for the cars that are being serviced in our garage will be ordered directly from Japan or Germany. 
                        We will display regular things like: Engine Oil, Brake Fluid, ATF in our Mini Shop.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            What type of car models do you cater for repair?
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="faqTwo" data-parent="#sgFAQ">
                    <div class="card-body">
                        We only cater quick fix repairs and replacements for Japanese and European Cars.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            How many cars are allowed in one account?
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="faqThree" data-parent="#sgFAQ">
                    <div class="card-body">
                        The Member will get FREE carwash for 3 of their registered cars in our membership agreement with FREE check up.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqFour">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            After membership expires, how many days would you give before it will be terminated?
                        </button>
                    </h2>
                </div>
                <div id="collapseFour" class="collapse" aria-labelledby="faqFour" data-parent="#sgFAQ">
                    <div class="card-body">
                        Each member will be given 15 days prior notice for their Membership Due. Failure to pay the Monthly or Yearly Dues, will be subject for termination within 15 business days.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqFive">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                            Can I bring a Sa Garahe member to my room (VIP/Elite)?
                        </button>
                    </h2>
                </div>
                <div id="collapseFive" class="collapse" aria-labelledby="faqFive" data-parent="#sgFAQ">
                    <div class="card-body">
                        Every member is allowed to bring anyone in their VIP/Elite Rooms, as long they have pre-registered their visitors in our Daily Schedule List.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqSix">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                            Can I bring non-member visitors to my room (VIP/Elite)?
                        </button>
                    </h2>
                </div>
                <div id="collapseSix" class="collapse" aria-labelledby="faqSix" data-parent="#sgFAQ">
                    <div class="card-body">
                        Every member is allowed to bring anyone in their VIP/Elite Rooms, as long they have pre-registered their visitors in our Daily Schedule List.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqSeven">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                            Can I switch to another room?
                        </button>
                    </h2>
                </div>
                <div id="collapseSeven" class="collapse" aria-labelledby="faqSeven" data-parent="#sgFAQ">
                    <div class="card-body">
                        Yes, if there is available room.
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="faqEight">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                            Can I customize my room according to my style or needs?
                        </button>
                    </h2>
                </div>
                <div id="collapseEight" class="collapse" aria-labelledby="faqEight" data-parent="#sgFAQ">
                    <div class="card-body">
                        We will just setup and build the Walls, Ceiling and Flooring. The Member is allowed to either set up their own style, or we will create the structure of their office based on their desire and needs.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f536b8ba9247079"></script>
</section>