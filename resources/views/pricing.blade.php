@extends('layouts.app')
@section('content')
<!-- Pricing Section Begin -->
<section class="pricing-section spad">
    <div class="container">
        <div class="row mt-9">
            <div class="col-lg-12">
                <div class="section-title">
                    <span class="title-description">Pricing built for all Your needs</span>
                    <h1 class="mt-2">Choose your plan</h1>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Regular</h3>
                    <div class="pi-price">
                        <h2>₱15,000</h2>
                        <span>3 Seats</span><br/><br/>
                        <span>6 Tables</span><br/>
                        <span>(5 Seats/tables)</span>
                    </div>
                    <ul>
                        <li>5 Day Pass a week to your allocated</li>
                        <li>Seat and table</li>
                        <li>Car wash and clean</li>
                        <li>Reshuffle every 3 weeks</li>
                    </ul>
                    <a href="/get-quote/Regular" class="secondary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Elite</h3>
                    <div class="pi-price">
                        <h2>₱60,000</h2>
                        <span>6 Seats</span><br/><br/>
                        <span>3 Rooms + 1 Elite Cube</span>
                    </div>
                    <ul>
                        <li>Room Butler Service</li>
                        <li>25% Discount to all orders</li>
                        <li>5 days pass for Elite Rooms and 2 days</li>
                        <li>Pass for Elite Cubicles a week</li>
                        <li>Car wash and clean</li>
                    </ul>
                    <a href="/get-quote/Elite" class="primary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-8">
                <div class="ps-item">
                    <h3>Vip</h3>
                    <div class="pi-price">
                        <h2>₱45,000</h2>
                        <span>6 Seats</span><br/><br/>
                        <span>6 Rooms + 3 vip Cubes</span>
                    </div>
                    <ul>
                        <li>Car wash and clean</li>
                        <li>5 days pass for VIP Rooms and 2 days</li>
                        <li>Pass for VIP Cubicles a week</li>
                        <li>Car wash and clean</li>
                    </ul>
                    <a href="/get-quote/VIP" class="secondary-btn btn-normal pricing-btn">Register now</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Pricing Section End -->
<!-- Banner Section Begin -->
<section class="banner-section">
    <div class="banner-bg">
        <div class="row">
            <div class="col-12 col-md-6 flex-grow-1 mb-4">
                <h1>Upgrade your plan</h1>
                <p>Talk to one of our agents to upgrade your current plan.</p>
            </div>
            <div class="col-12 col-md-6 banner-buttons">
                <a href="/get-quote" class="primary-btn btn-normal mb-3 mr-lg-1" role="button">Upgrade Plan</a>
                <a href="/contact" class="secondary-btn btn-normal mb-3 ml-lg-1" role="button">Contact us</a>
            </div>
        </div>
    </div>
</section>
<!-- Banner Section End -->

@include('faq');

@endsection