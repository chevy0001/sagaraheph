@extends('layouts.app')
@section('content')
<!-- Free Quote Section Begin -->
<section class="team-section spad">
    <div class="blog-container mt-9">
        <div class="container-fluid">
            <div class="row services">
                <div class="col-12 col-lg-6 mb-4">
                    <h1>Contact Us</h1>
                    <p>
                        Provide your details in the form and click contact us. We will reply to you as soon as possible. Thank you.
                    </p>
                    <p class="mb-0">Follow us</p>
                    <div class="fa-social">
                        <a href="https://www.facebook.com/sagaraheph"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.twitter.com/SGarahe"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                        <a href="https://www.instagram.com/sagaraheph/"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-12 col-lg-6 px-0 free-quote-form">
                    <form method="post" action="/leads">
                    @csrf
                        <div class="form-group">
                            <label for="name">Your Name*</label>
                            <input type="text" class="form-control" id="name" name="name" required>
                            <div id="nameInvalidFeedback" class="invalid-feedback">
                                Please enter your name.
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phoneNo">Phone No.*</label>
                            <input type="phone" name="phoneNo" class="form-control" id="phoneNo" required>
                          </div>
                        <div class="form-group">
                            <label for="emailAddress">Email address*</label>
                            <input type="email" name="emailAddress" class="form-control" id="emailAddress" required>
                        </div>
                        <div class="form-group">
                            <label for="clientMessage">Message/Questions</label>
                            <textarea class="form-control" name="clientMessage" id="clientMessage"></textarea>
                        </div>
                        <button type="submit" class="primary-btn btn-normal btn mt-4">Contact Us</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Free Quote Section End -->
<!-- Contact Section Begin -->
<section class="banner-section">
    <div class="banner-bg">
        <div class="row">
            <div class="col-12 col-xl-4">
                <div class="gt-text">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/location.svg') }}">
                    </div>
                    <p class="mb-0">Alzate St. Bo. Obrero, <br/>Davao City, Philippines</p>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="gt-text">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/contact.svg') }}">
                    </div>
                    <p class="mb-0">(+63) 9953574834<br/> (082) 333-4203</p>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="gt-text email">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/email.svg') }}">
                    </div>
                    <p class="mb-0 pt-0">support@sagarahe.com</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Section End -->
<!-- Pricing Section End -->
@include('faq');
@endsection