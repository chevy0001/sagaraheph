@extends('layouts.app')
@section('content')
<!-- Free Quote Section Begin -->
<section class="team-section spad">
    <div class="blog-container mt-9">
      @if (session('status'))
          <div class="alert alert-success text-center" id="alert">
              {{ session('status') }}
          </div>
        @endif
        <div class="container-fluid">
            <div class="row services">
                <div class="col-12 col-lg-6 mb-4">
        
                    <h1>Get Quote</h1>
                    <p>
                        Provide your details in the form and click contact us. We will reply to you as soon as possible. Thank you.
                    </p>
                    <p class="mb-0">Follow us</p>
                    <div class="fa-social">
                        <a href="https://www.facebook.com/sagaraheph"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.twitter.com/SGarahe"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                        <a href="https://www.instagram.com/sagaraheph/"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
                <div class="col-12 col-lg-6 px-0 free-quote-form">
                    @include('qoute');

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Free Quote Section End -->
<!-- Contact Section Begin -->
<section class="banner-section">
    <div class="banner-bg">
        <div class="row">
            <div class="col-12 col-xl-4">
                <div class="gt-text">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/location.svg') }}">
                    </div>
                    <p class="mb-0">Obrero, Davao City,<br/>Philippines,<br/>8000</p>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="gt-text">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/contact.svg') }}">
                    </div>
                    <p class="mb-0">(+63) 912 345 6789<br/> (+82) 123 4567</p>
                </div>
            </div>
            <div class="col-12 col-xl-4">
                <div class="gt-text email">
                    <div class="contact-icon mr-3">
                        <img src="{{ asset('images/email.svg') }}">
                    </div>
                    <p class="mb-0 pt-0">support@sagarahe.com</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Contact Section End -->
<!-- Pricing Section End -->
@include('faq');

@endsection

<script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script>
$(function(){
    //status alert   
    setTimeout(function(){
        $('#alert').fadeOut('slow');
    },5000);

});
