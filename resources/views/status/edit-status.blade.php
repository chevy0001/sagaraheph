@extends('layouts.admin')

@section('content')
<section class="client-spad">
    <div class="container-fluid mt-9 mb-9">
        <div class="row">
            <div class="col-12 col-md-3">

                @include('partials.admin-sidebar')
            
            </div>
            <div class="col-12 col-md-9 bg-white ">
              <div class="p-md-5 pt-5 pb-5">
                <div class="row container-fluid">
                  <div class="user-form col-md-8 mx-auto">
                      <h3 class="justify-content-center">
                        Add a plan
                      </h3>
                      @if($message)
                          <div class="primary-btn btn-normal col-sm-12 mx-auto">{{$message->message}} | <a class="py-0" href="/user-profile/{{$tokenid}}">Profile</a></div>
                        @endif
                    <form method="post" action="/store-status/{{$tokenid}}">
                        @csrf
                      <div class="form-group mt-4">
                          <label for="status" class="col-form-label">Select a Plan</label>
                          <div>
                            <select id="status" name="status" type="text" class="form-control">
                              <option value="Regular">Regular - 15K/Seat</option>
                                <option value="VIP">VIP - 45K/Seat</option>
                                <option value="Elite">Elite - 60K/Seat</option>
                            </select>
                          </div>
                      </div>
                      <div class="form-group">
                          <label for="seats" class="col-form-label">Number of Seats</label>
                          <div>
                              <input class="form-control" type="number" name="seats" id="seats" min="1" value="1">
                          </div>
                      </div>
                      <div class="form-group text-center mt-5">
                          <button type="submit" class="primary-btn btn-normal">
                          
                            @if(auth()->user()->role === '1')     
                              {{ __('Add Plan') }}
                            @else
                              {{ __('Request Upgrade') }}
                            @endif  
                          
                          </button>
                      </div>
                    </form>
                  </div>
                 </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection