<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Global site tag (gtag.js) - Google Analytics -->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T3F82X9');</script>
<!-- End Google Tag Manager -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-177208446-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-177208446-1');
</script>
    <meta property="og:image" content="{{$image ?? ''}}">
    <meta name="twitter:image" content="{{$image ?? ''}}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="{{$content ?? 'website'}}">
    <meta property="og:title" content="{{$title ?? 'Sa Garahe - Chill and Relax'}}">
    
    @if(@isset($description))
    <meta property="og:description" content="{!! $description !!}">
    @else
    <meta property="og:description" content="Aside from quality carwash service your Sagarahe have complete set of spare parts, tools and best staff that will cater to your need...">
    @endif
    <meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
    <meta property="og:url" content="{{$url ?? 'https://www.sagarahe.com'}}">
    <meta property="og:site_name" content="SaGarahe.com">
    <meta name="keywords" content="Davao Garage, garahe, chill, relax, vip room, exclusive room, board rooms, for men only place, davao city, elite place, coffee shop, carwash, car painting, car detailing, barbershop, garage"> 
    <meta property="article:publisher" content="https://www.facebook.com/sagaraheph">

    
    
    <meta property="image:height" content="200">
    <meta property="image:width" content="400">

    <meta name="twitter:card" content="summary">
    @if(@isset($description))
    <meta name="twitter:description" content="{!! $description !!}">
    @endif
    <meta name="twitter:title" content="{{$title ?? 'Sa Garahe - Chill and Relax'}}">
    <meta name="twitter:site" content="SGarahe">

    
    
    
    <meta name="twitter:creator" content="SGarahe">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @if(@isset($title))
         Sa Garahe - {{$title ?? ''}}
        @else
        Sa Garahe - Men's Safe Haven
        @endif 
    </title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    
    
    <!-- Styles -->
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/flaticon.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/barfiller.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/slicknav.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/png" href="images/sagarahe.png"/>
   <!-- asset('images/services-a.jpg') }} -->
    
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T3F82X9"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!-- Search model Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch">+</div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    @yield('core')
    
    <!-- Js Plugins -->
    {{-- <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('js/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('js/jquery.barfiller.js') }}"></script>
    <script src="{{ asset('js/jquery.slicknav.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script> --}}
    <!-- Go to www.addthis.com/dashboard to customize your tools -->


</body>
</html>
