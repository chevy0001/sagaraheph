@extends('layouts.base')

@section('core')
    <div id="app">
        @include('partials.user-nav')
        <main>
        	{{ TawkTo::widgetCode() }}
            @yield('content')
        </main>
        @include('partials.footer')
    </div>
@endsection