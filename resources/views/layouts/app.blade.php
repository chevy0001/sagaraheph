@extends('layouts.base')

@section('core')
    <div id="app">
        @include('partials.nav')
        <main>
            @yield('content')
            {{ TawkTo::widgetCode() }}
        </main>
        @include('partials.footer')
    </div>
@endsection