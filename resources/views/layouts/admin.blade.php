@extends('layouts.base')

@section('core')
    <div id="app">
        @include('partials.user-nav')
        <main>
            @yield('content')
        </main>
        @include('partials.footer')
    </div>
@endsection