<div class="side-desktop">
    <ul class="nav flex-column">
        <li class="nav-item">
          <a class="nav-link" href="/admin/{{auth()->user()->token_id}}">Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/user-list">Users</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/blogs">Blog</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/enquiries">Enquiries</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/invoice">Invoice</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/notification">Notification 
            @if(@isset($notif_count))
            <span class="notification">({{$notif_count ?? ''}})</span>
            @endif
          </a>
        </li>
    </ul>
    </div>
    
    <div class="side-mobile mb-4">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="/user/{{auth()->user()->token_id}}">Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/payment-history/{{auth()->user()->token_id}}">Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/blogs">Blog</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/enquiries">Enquiries</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Invoice</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Notification</a>
            </li>
        </ul>
    </div>