<!-- Offcanvas Menu Section Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="offcanvas-menu-wrapper">
    <div class="canvas-close">
        <i class="fa fa-close"></i>
    </div>
    <div class="canvas-search search-switch">
        <i class="fa fa-search"></i>
    </div>


    <nav class="canvas-menu mobile-menu mb-3">
        <ul>

            <li><a class="py-0 nav-login" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="">
                    @csrf
                  </form>
        </ul>
    </nav>
    <div id="mobile-menu-wrap">
    </div>
</div>
<!-- Offcanvas Menu Section End -->

<!-- Header Section Begin -->
<header class="header-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">

                <div class="logo">
                    <a href="/">
                        <img src="{{ asset('images/sg-logo.svg')}}" alt="SagarahePH Logo">
                    </a>
                </div>
            </div>
            <div class="col-lg-9">
                <nav class="nav-menu text-right">
                    <ul>
                        @if(auth()->user()->role === '0')
                        <li><a class="py-0" href="/user/{{auth()->user()->token_id}}">{{auth()->user()->firstname}}</a></li>
                        @else
                        <li><a class="py-0" href="/admin/{{auth()->user()->token_id}}">{{auth()->user()->firstname}}</a></li>
                        @endif
                        <li><a class="py-0" href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>
            </li>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="">
                    @csrf
                  </form>
                    </ul>
                </nav>
            </div>

        </div>
        <div class="canvas-open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
<!-- Header End -->