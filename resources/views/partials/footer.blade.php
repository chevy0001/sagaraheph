<!-- Footer Section Begin -->
<section class="footer-section">
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-lg-4">
                <div class="fs-about">
                    <div class="fa-logo">
                        <a href="/"><img src="{{ asset('images/sg-logo.svg')}}" alt="SagarahePH Logo"></a>
                    </div>
                    <p>Sagarahe is a place for big boys, it is a men's safe haven, where men can relax, fix their cars, talk with like-minded people.</p>
                    <div class="fa-social">
                        <a target='_blank' href="https://www.facebook.com/sagaraheph"><i class="fa fa-facebook"></i></a>
                        <a href="http://twitter.com/intent/follow?source=followbutton&variant=1.0&screen_name=SGarahe" target="_blank"> <i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-youtube-play"></i></a>
                        <a href="https://www.instagram.com/sagaraheph/" target="_blank"><i class="fa fa-instagram"></i></a>
                        <a href="/contact"><i class="fa  fa-envelope-o"></i></a>
                    
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-2 col-md-3 col-sm-6">
                <div class="fs-widget">
                    <h4>Services</h4>
                    <ul>
                        <li><a href="/services/1">Service A</a></li>
                        <li><a href="/services/2">Service B</a></li>
                        <li><a href="/services/3">Service C</a></li>
                        <li><a href="/services/4">Service D</a></li>
                    </ul>
                </div>
            </div>--}}
            <div class="col-lg-2 col-md-3 col-sm-6">
                <div class="fs-widget">
                    <h4>Resources</h4>
                    <ul>
                        <li><a href="/about">About</a></li>
                       {{-- <li><a href="/blog">Blog</a></li>--}}
                        <li><a href="/pricing">Pricing</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-6">
                <div class="fs-widget">
                    <h4>Support</h4>
                    <ul>
                        <li><a href="/login">Login</a></li>
                        <li><a href="/contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="d-flex">
            <div class="flex-grow-1">
                <p>Privacy - Terms & Conditions</p>
            </div>
            <div class="text-right">
                <p>All Rights Reserved © 2020</p>
            </div>
        </div>
    </div>
</section>
<!-- Footer Section End -->