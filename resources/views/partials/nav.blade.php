{{ TawkTo::widgetCode() }}
<div class="banner">
    
    @if(@isset($remaining))
        <div class="container py-3">
            <p class="mb-0">Hurry! There are only {{-- $remaining --}}10 rooms/seats remaining! <a href="/get-quote">Register Now!</a></p>
        </div>
    @endif
</div>
<!-- Offcanvas Menu Section Begin -->
<div class="offcanvas-menu-overlay"></div>
<div class="offcanvas-menu-wrapper">
    <div class="canvas-close">
        <i class="fa fa-close"></i>
    </div>
    <div class="canvas-search search-switch">
        <i class="fa fa-search"></i>
    </div>

    <nav class="canvas-menu mobile-menu mb-3">
        <ul>
            <li><a href="/services">Services</a></li>
            <li><a href="/pricing">Pricing</a></li>
            <li><a href="/about">About</a></li>
            {{--<li><a href="/blog">Blog</a></li>--}}
            <li><a href="/contact">Contact</a></li>
            <li><a href="/login">Login</a></li>
        </ul>
    </nav>
    <div id="mobile-menu-wrap">
    </div>
    <a href="/get-quote" type="button" class="primary-btn btn-normal mt-4">Get Quote</a>
</div>
<!-- Offcanvas Menu Section End -->

<!-- Header Section Begin -->
<header class="header-section mt-5">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">

                <div class="logo">
                    <a href="/">
                        <img src="{{ asset('images/sagarahe-logo.svg')}}" width="100px" alt="SagarahePH Logo">
                    </a>
                </div>
            </div> 
            <div class="col-lg-6">
                <nav class="nav-menu">
                    <ul>
                        <li><a href="/services">Services</a></li>
                        <li><a href="/pricing">Pricing</a></li>
                        <li><a href="/about">About</a></li>
                        {{--<li><a href="/blog">Blog</a></li>--}}
                        <li><a href="/contact">Contact</a></li>
                    </ul>
                </nav>
            </div>
            
            <div class="col-lg-3 text-right">
                <div class="top-option">
                    <div class="to-search search-switch">
                        <i class="fa fa-search"></i>
                    </div>

          
            @if(Auth::user())

              @if(Auth::user()->role === '0')

               <a href="/user/{{Auth::user()->token_id}}" class="nav-login">{{Auth::user()->firstname}}</a>
              @else
                <a href="/admin/{{Auth::user()->token_id}}" class="nav-login">{{Auth::user()->firstname}}</a>
              @endif  

            @else
              <a href="/login" class="nav-login">Login</a>
              <a href="/get-quote" class="primary-btn-small btn-normal mt-n-11">Get Quote</a>
            @endif

          
                </div>
            </div>


            {{--Start of of Guess --}}


            {{--End of --}}

        </div>
        <div class="canvas-open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
<!-- Header End -->