<div class="side-desktop">
<ul class="nav flex-column">
    <li class="nav-item">
        <a class="nav-link" href="/user/{{auth()->user()->token_id}}">Dashboard</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/payment-history/{{auth()->user()->token_id}}">Payments</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/edit-profile/{{auth()->user()->token_id}}">Edit Account</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/add-status/{{auth()->user()->token_id}}">Upgrade Plan</a>
    </li>
    <li class="nav-item">
                <a class="nav-link" href="/notification">Notification</a>
    </li>
     <li class="nav-item">
            <a class="nav-link" href="/invoice/{{auth()->user()->token_id}}">Invoice</a>
     </li>
</ul>
</div>

<div class="side-mobile mb-4">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="/user/{{auth()->user()->token_id}}">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="payment-history/{{auth()->user()->token_id}}">Payments</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/edit-profile/{{auth()->user()->token_id}}">Edit Account</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="/add-status/{{auth()->user()->token_id}}">Upgrade Plan</a>
    </li>
    <li class="nav-item">
                <a class="nav-link" href="/notification">Notification</a>
        </li>
    </ul>
</div>