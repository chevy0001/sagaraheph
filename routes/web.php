<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...

Route::get('/sagarahestaff/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
	Route::post('/sagarahestaff/register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::patch('/password/resetpass', 'Auth\ResetPasswordController@reset');

//************************************************

Route::post('/leads', 'EmailController@store');



//************Pages*******************//
Route::post('/subscribe', 'EmailController@newsletter');
Route::get('/', 'HomeController@homepage');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth.basic');

Route::get('/blog/{slug}', 'BlogController@blogPost');
Route::get('/blog', 'BlogController@show');
Route::get('/services', 'HomeController@services');
Route::get('/services/1', 'HomeController@servicesPost');
Route::get('/pricing', 'HomeController@pricing');
Route::get('/about', 'HomeController@about');
Route::get('/contact', 'HomeController@contact');
Route::get('/get-quote/{plan?}', 'HomeController@getQuote');

//**************************************//


//************************Admin*************************************************//
Route::group(['middleware' => 'App\Http\Middleware\Admin'], function()
{
	
	Route::post('/store-user', 'ProfileController@store')->middleware('auth.basic');
	Route::get('/register-user', 'ProfileController@create')->middleware('auth.basic');
	Route::get('/user-list', 'ProfileController@list')->name('userlist');
	Route::get('/user-profile/{tokenid}', 'ProfileController@show')->middleware('auth.basic');

	Route::get('/admin/{tokenid}', 'AdminController@index')->middleware('auth.basic');

	//Edit individual user
	Route::get('/edit/{tokenid}', 'UserController@edit')->name('edit-user')->middleware('auth.basic');
	Route::patch('/profile-edit/{tokenid}', 'UserController@update')->middleware('auth.basic');

	//Status Controller
	Route::get('/edit-status/{tokenid}', 'StatusController@edit')->middleware('auth.basic');

	//Payment Controller
	Route::patch('/update-payment/{tokenid}', 'PaymentHistoryController@update')->middleware('auth.basic');
	

	//Notifications
	

	// Enquiries
	Route::get('/enquiries', 'EnquiriesController@index')->middleware('auth.basic');
	Route::get('/enquiries-show', 'EnquiriesController@showEnquiries')->middleware('auth.basic');

	//Blog
	Route::post('/store-blog', 'BlogController@store')->middleware('auth.basic');
	Route::patch('/update/{id}', 'BlogController@update')->middleware('auth.basic');
	Route::get('/blogs', 'AdminController@blogs')->middleware('auth.basic');
	Route::get('/add-blog', 'BlogController@create')->middleware('auth.basic');
	
	Route::get('/edit-blog/{id}', 'BlogController@editBlog')->middleware('auth.basic');

});


Route::get('/notification', 'NotificationController@index')->middleware('auth.basic');

Route::get('/add-status/{token_id}', 'StatusController@index')->middleware('auth.basic');
Route::post('/store-status/{token_id}', 'StatusController@store')->middleware('auth.basic');

//********************************************End of Admin**********************************

//*****************************User Routes***************************************************
Route::post('/store-reciept/{tokenid}', 'PaymentHistoryController@store')->middleware('auth.basic');
Route::get('/user/{tokenid}', 'UserController@index')->middleware('auth.basic');
Route::get('/edit-profile/{tokenid}', 'UserController@editUser')->middleware('auth.basic');
Route::get('/add-payment/{tokenid}/{invoice}', 'PaymentHistoryController@create')->middleware('auth.basic');
Route::get('/payment-history/{tokenid}', 'PaymentHistoryController@show')->middleware('auth.basic');
//****************************End of User Routes**************************************************

//Invoice
	Route::get('/invoice/{tokenid?}', 'InvoiceController@index')->middleware('auth.basic');
	Route::get('/invoice-show/{tokenid}/{invoiceNo}', 'InvoiceController@showInvoice')->middleware('auth.basic');




Route::get('/refresh', function () {
  Artisan::call('view:clear');
  return back();
});

Route::get('/storage-link', function () {
  Artisan::call('storage:link');
  return back();
});

Route::get('/db-seed-users', function () {
  Artisan::call('db:seed --class=UserSeeder');
  return back();
});

Route::get('/sitemap.xml', 'Sitemap@sitemap');
 
