<?php

namespace App\Http\Middleware;
use Illuminate\Http\Response;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->role !== '1')
            {
            //return new Response(view('unauthorized.unauthorized')->with('role', 'Admin'));
              return redirect('/user/'.auth()->user()->token_id);  
            }  
        return $next($request);
    }
}
