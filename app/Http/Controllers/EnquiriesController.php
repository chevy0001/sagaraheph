<?php

namespace App\Http\Controllers;
use App\Email;
use DB;
use Illuminate\Http\Request;

class EnquiriesController extends Controller
{
    public function index()
    {
    	$enq = DB::table('emails')->orderBy('created_at', 'DESC')->paginate(10);
    	
        return view('admin.enquiries',compact('enq'));
    }

    public function showEnquiries()
    {

    	//Query emails here sort from newest to oldest, paginate by 10

    	


        return view('admin.enquiries-show');
    }
}
