<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;
use DB;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $imgUrl = 'https://sagarahe.com/';
    public function index()
    {


        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        return view('admin.add-blog');
    }

   


    public function blogPost($slug)
    {

      //$blogs = Blog::where('slug',$slug)->firstOrFail();


      $blogs = DB::table('blogs')
           ->where('slug',$slug)
           ->first();
          //  dd($blogs);
           if(!isset($blogs)){
            return redirect('/blog');
           }
      $title = $blogs->title;
      $description = strip_tags(htmlspecialchars_decode(substr($blogs->content, 0,112))).'...';
      $content = 'article';      
      $image = $this->imgUrl.'storage/'.$blogs->image;
      $url = $this->imgUrl.'blog/'.$blogs->slug;

      //dd($image);
      return view('blog-post',compact('blogs','title','description','image','content','url'));
     
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = request()->validate([
              'title' => 'required',
              'content'=>['required'],
              'image'=>'',
      
      ]);

      
      $blog = new \App\Blog();

      $blog->title = $data['title'];
      
      $blog->content = $data['content'];

      $blog->slug = Str::slug($blog->title, '-').'-'.rand(1000,9999);
      $blog->author_id = auth()->user()->id;
      //dd($blog->slug);
      if(request('image')!==null){ 
            $image_path = request('image')->store('uploads','public');
            $image = Image::make(public_path("storage/{$image_path}"));
          
            $image->save();
        }
        
        $blog->image = $image_path;



      $blog->save();
      return redirect('/blogs');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show()
    {


      $title = 'Blogs';
      $remaining = StatusController::countSeats();


//$latest = App\Blog::latest()->firstOrFail();
      $latest = DB::table('blogs')->latest()->first();
      if($latest){
      $description = strip_tags(htmlspecialchars_decode(substr($latest->content, 0,150))).'...';
      $image = $this->imgUrl.'storage/'.$latest->image;
      }
      else{
        $description = '';
        $image = '';
      }
      $url = $this->imgUrl.'blog';
      
     // dd(strip_tags(htmlspecialchars_decode($description)));

      $blogs = DB::table('blogs')->paginate(8);
      if($latest){
      return view('blog',compact('latest','blogs','remaining','title','description','image','url'));
      }
      else{
        return redirect('/');
      }
    }


     public function editBlog($id)
    {
        $sulod = DB::table('blogs')
                ->where('id',$id)
                ->first(); 
         
       // dd($contents);
        return view('admin.edit-blog',compact('sulod'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        $data = request()->validate([
              'title' => 'required',
              'content'=>['required'],
              'image'=>'',
      
      ]);

      
      $blog = new \App\Blog();

      $blog->title = $data['title'];
      
      $blog->content = $data['content'];


      $blog->slug = Str::slug($blog->title, '-').'-'.rand(1000,9999);
      $blog->author_id = auth()->user()->id;
      //dd($blog->slug);
      if(request('image')!==null){ 
            $image_path = request('image')->store('uploads','public');
            $image = Image::make(public_path("storage/{$image_path}"));
          
            $image->save();
        }
        
        $blog->image = $image_path;

      $blog->save();
      return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update( Blog $blog,$id)
    {
     
     $blog = Blog::find($id);
    // dd($blog->id);
      $data = request()->validate([
              'title' => 'required',
              'content'=>['required'],
              'image'=>'',
      
      ]);

      if(request('image')!==null){ 
            $image_path = request('image')->store('uploads','public');
            $image = Image::make(public_path("storage/{$image_path}"));
            $image->save();


        }
        else{
            $image_path = $blog->image;
            $data['profile_photo'] = $image_path ;
        }

        if($blog->title === $data['title']){
        $blog->title = $data['title'];
        $blog->slug = $blog->slug;
        }
        else{
          $blog->title = $data['title'];
          $blog->slug = Str::slug($blog->title, '-').'-'.rand(1000,9999);
        }
        

        
        
        $blog->image = $image_path;
        $blog->content = $data['content'];

        $blog->save();
        return redirect('/blog/'.$blog->slug);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }
}
