<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Invoice;
use App\User;
use App\Status;
use App\PaymentHistory;

class InvoiceController extends Controller
{
  public function index($tokenid = null)
  {


    if(auth()->user()->role === '1'){
      


      $invoices = DB::table('statuses')
              ->leftJoin('payment_histories','payment_histories.invoice_no','=','statuses.invoice_number')
              ->leftJoin('users','users.token_id','=','statuses.user_token_id')

      ->latest('statuses.created_at')
      ->get();
      //dd($invoices);
    }
    else{
  	$user = User::where('token_id',$tokenid)->firstOrFail(); 
    $invoices = DB::table('statuses')
              ->leftJoin('payment_histories','payment_histories.invoice_no','=','statuses.invoice_number')
      ->where('statuses.user_token_id',$tokenid)              
      ->latest('statuses.created_at')
      ->get();
     }


   
      
      return view('admin.invoice-list',compact('invoices'));
  }

  public function showInvoice($tokenid,$invoiceNo)
  {


   $user = User::where('token_id',$tokenid)->firstOrFail(); 
  	$invoice = Status::where('invoice_number',$invoiceNo)
  						->firstOrFail();
    // dd($invoice);         					
      return view('admin.invoice',compact('invoice','user'));
  }
}


