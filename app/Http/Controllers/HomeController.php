<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\StatusController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    private $imgUrl = 'https://sagarahe.com/';
    public function index()
    {
      return view('home');
    }
    public function homepage()
    {
      $remaining = StatusController::countSeats();

      $description = 'Experience high quality carwash services. Spare parts and tools are available for your convenience. And have coffee with friends and acquaintances.';
      
      $image = $this->imgUrl.'images/cover-img.jpg';
      $url = $this->imgUrl;

        return view('index',compact('remaining','description','image','url'));
    }
    public function services()
    { 

      $title = 'Services';
      $description = 'Aside from quality carwash service your garahe have complete set of spare parts, tools, car painting and detailing services and most importantly best staff that will cater to your needs.';
      $remaining = StatusController::countSeats();

      $image = $this->imgUrl.'images/cover-img.jpg';
      $url = $this->imgUrl.'pricing';
      return view('services',compact('remaining', 'title','description','image','url'));

      
    }
    public function servicesPost()
    {
      return view('services-post');
    }
    public function pricing()
    {
      $title = 'Pricing';
      $description = 'Room Butler Service, 25% Discount to all orders, 5 days pass for Elite Rooms and 2 days, Pass for Elite Cubicles a week, Car wash and clean';
      $remaining = StatusController::countSeats();
      $image = $this->imgUrl.'images/cover-img.jpg';
      $url = $this->imgUrl.'pricing';
      return view('pricing',compact('remaining','title', 'description','image','url'));
    }
    public function about()
    {
      $title = 'About';
      $description = 'Sa Garahe, is founded by the head car wash boy Mr. Roman Linejan, he also founded StartUpz Co-Working Space, Financial Consultant at...';
      $url = $this->imgUrl.'about';
      //$image = '';
      $remaining = StatusController::countSeats();
      $image = $this->imgUrl.'images/cover-img.jpg';
      return view('about',compact('remaining','description','title','url','image'));
    }
    public function contact()
    {
      $title = 'Contact';
      $description = 'Location: Obrero Davao City. Phone: (+63) 9953574834, (082) 333-4203. Email: support@sagarahe.com';
      $remaining = StatusController::countSeats();
      $image = $this->imgUrl.'images/cover-img.jpg';
      $url = $this->imgUrl.'contact';
      return view('contact',compact('remaining','title','description','image','url'));
    }
    public function getQuote($plan = null)
    {
      $image = $this->imgUrl.'images/cover-img.jpg';
      $url = $this->imgUrl.'get-quote';
      $title = 'Get Qoute';
      $remaining = StatusController::countSeats();
      $description = 'Be informed from our crew about the services and prices we offer.';


      $plan = strtoupper($plan);
      //dd(strcmp($plan,'vip'));
     




      return view('get-quote',compact('remaining','image','url','title','description','plan'));
    }
    
    
}
