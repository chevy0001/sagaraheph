<?php

namespace App\Http\Controllers;

use App\Status;
use App\Invoice;
use App\Notification;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($tokenid)
    {

        return view('status.dashboard',compact('tokenid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$tokenid)
    {
   $status = new Status;
   $notif = new Notification;
   $invoice = new Invoice;
   //dd(auth()->user()->role);

   //Store or Update Status
   if(auth()->user()->role === '1'){
     
     Notification::where('is_read', '0')
      ->where('client_token_id', $tokenid)
      ->update(['is_read' => '1']);

      $invoice->admin_id = auth()->user()->id;



   }
   else{
   
    //Send Notif to admin if their is an upgrade request
    $notif->user_id = auth()->user()->id;
    $notif->client_token_id = auth()->user()->token_id;
    $notif->notif_type = 'Upgrade Request';
    $notif->message = auth()->user()->firstname.' '.auth()->user()->lastname.' submitted an upgrade request. Membership: '.$request->status.'. Seats: '.$request->seats;
    $notif->is_read = 0;
    $notif->link = '/edit-status/'.$tokenid; 
    $notif->save();

    $invoice->admin_id = 0;
   }

   $status->upgrade_request = '0';
   $status->admin_id = auth()->user()->id;
   $status->user_token_id = $tokenid;
   $status->status_name = $request->status;
   $status->seats = $request->seats;

    
    $invoice->invoice_number =  'SGRHPH'.Str::random(2).rand(1000,9999); 
    $invoice->client_token_id = $tokenid;
    $invoice->status_name = $request->status;
    $invoice->save();
     
    $status->invoice_number = $invoice->invoice_number;


     $price = 0;
     if($status->status_name === "Regular")
      $price = 15000;
     else if($status->status_name === "VIP")
      $price = 45000;
    else if ($status->status_name === "Elite")
      $price = 60000;
    else
      $price = 0;
     
     $total =  (int)$request->seats * $price;
     
     $status->total = $total;

     //dd($status);

     $status->save();

   
   if(auth()->user()->role === '1'){
    return redirect('/invoice-show/'.$tokenid.'/'.$invoice->invoice_number)->with('status', 'Plan Updated');
   }
   else{
    return redirect('/invoice-show/'.$tokenid.'/'.$invoice->invoice_number)->with('status', 'Request Sent');
       }
       
    }


    public static function countSeats(){

      $count = DB::table('statuses')
                 ->sum('seats');  
    // $count_vip = DB::table('statuses')
    //             ->where('status_name', 'VIP')
    //             ->sum('seats');  
    // $count_elite = DB::table('statuses')
    //             ->where('status_name', 'Elite')
    //             ->sum('seats'); 
    // $count_regular = DB::table('statuses')
    //             ->where('status_name', 'Regular')
                //->sum('seats');  
    $remaining = 15 - $count;
    return $remaining;

    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function show(Status $status)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function edit($tokenid)
    {

        
        // $message = DB::table('notifications')
        //            ->where('is_read','0')
        //            ->where('client_token_id',$tokenid)
        //            ->latest()
        //            ->firstOrFail();

        //$model = App\Flight::where('legs', '>', 100)->firstOrFail();
        $message = Notification::where('is_read','0')
                        ->where('client_token_id',$tokenid)
                        ->orderBy('created_at','desc')
                        ->firstOrFail();

        //dd($message);
        return view('status.edit-status',compact('tokenid','message'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Status $status)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Status $status)
    {
        //
    }
}
