<?php

namespace App\Http\Controllers;

use App\PaymentHistory;
use App\Notification;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use DB;
use App\Status;
class PaymentHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tokenid)
    {
        //return view('payments.payment-history',compact('tokenid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tokenid,$invoiceNo)
    {
        $invoice = DB::table('statuses')
        ->where('user_token_id',$tokenid)
        ->where('invoice_number',$invoiceNo)
        ->first();

        if(!$invoice){
          return redirect('/user/'.auth()->user()->token_id);
        }
        //dd($invoice);
        return view('payments.payment-form',compact('tokenid','invoice'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$tokenid)
    {
       

        $image_path = request('reciept')->store('uploads','public');
        $image = Image::make(public_path("storage/{$image_path}"))->resize(309,null,function($constraint){
            $constraint->aspectRatio();
        });
        $image->save();

        $payment = new PaymentHistory;
        $payment->amount = $request->amount;
        $payment->receipt_no = $request->or_number;
        $payment->user_id = auth()->user()->id;//Client's id 
        $payment->admin_id = auth()->user()->id;//the one who uploaded the reciept
        $payment->client_token_id = $tokenid;
        $payment->receipt = $image_path;
        $payment->invoice_no = $request->invoice_number;
        //dd($payment->invoice_no);
        $payment->status_type = $request->status;
        if(auth()->user()->role == 0)
          $payment->is_verified = 0;
        elseif(auth()->user()->role == 1)
        $payment->is_verified = 1;
        else
        $payment->is_verified = 0;
        
        $payment->save();


        $notif = new Notification;
        $notif->user_id = auth()->user()->id;
        $notif->client_token_id = auth()->user()->token_id;
        $notif->notif_type = 'Reciept';
        $notif->message = auth()->user()->firstname.' '.auth()->user()->lastname.' Sent a receipt.';
        $notif->is_read = 0;
        $notif->link = '/user-profile/'.auth()->user()->token_id; 
        $notif->save();

        if(auth()->user()->role == 0)
          return redirect('/payment-history/'.$tokenid);
        else
         return redirect('/user-profile/'.$tokenid);  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function show($tokenid)
    {
        $paymentHistory = DB::table('payment_histories')
                   ->where('payment_histories.client_token_id',$tokenid)
                   ->orderBy('payment_histories.created_at', 'desc')
                   ->get();

        $status = DB::table('statuses')
                ->rightJoin('payment_histories','payment_histories.client_token_id','=','statuses.user_token_id')
                ->where('statuses.user_token_id',$tokenid)
                ->latest('statuses.created_at')
                ->first();   
                 
                  //dd($status);

        return view('payments.payment-history',compact('paymentHistory','tokenid','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentHistory $paymentHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $receipt_id)
    {

      $is_verified = PaymentHistory::where('id',$receipt_id)->first();
      $upgrade_request = Status::where('invoice_number',$is_verified->invoice_no)
      ->first();

      
      //dd($verify_payment);
        $is_verified->is_verified = $request->verify;
        //$is_verified->invoice_no = $update->invoice_number;//add invoice number 
        //dd($verify_payment->invoice_no);
        
        $upgrade_request->upgrade_request = $request->verify;

        //dd($update->upgrade_request);
        $upgrade_request->save();
        $is_verified->save();
        

        $notif = new Notification;
        $notif->user_id = $is_verified->user_id;
        $notif->client_token_id =$is_verified->client_token_id;
        $notif->notif_type = 'Verified';
        $notif->message = 'Admin verified your payment. Please check your dashboard to see the update';
        $notif->is_read = 0;
        $notif->link = '/user/'.$is_verified->client_token_id; 
        $notif->save();






        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentHistory  $paymentHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentHistory $paymentHistory)
    {
        //
    }
}
