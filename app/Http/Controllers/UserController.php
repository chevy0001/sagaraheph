<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Intervention\Image\Facades\Image;
use DateTime;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index($tokenId)
    {
      /*$profile = DB::table('users')
                   ->where('users.token_id',$tokenId)
                   ->first();*/
        $profile = User::where('users.token_id',$tokenId)->firstOrFail();

      $status = DB::table('statuses')
                   ->where('statuses.user_token_id',$tokenId)
                   ->latest()
                   ->first();                  
                   //dd($profile);
        
      $latestPayment = DB::table('payment_histories')
                   ->where('payment_histories.client_token_id',$tokenId)
                   ->latest()
                   ->first();


             
            $nextPayment = "Date Not Available";
            if(isset($latestPayment->created_at)){
              $nextPayment = date('M d, Y',strtotime($latestPayment->created_at.'30 Days' )); 
            }

             // $nextPayment = $latestPayment->created_at;
              //dd($nextPayment);    
      
      return view('user.dashboard',compact('profile','status','nextPayment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tokenid)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tokenId)
    {
      $profile = User::where('token_id',$tokenId)->firstOrFail();
      
      return view('admin.user-edit',compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        
        $user = DB::table('users')->where('id',$id)->first();

        $data = request()->validate([
          'firstname' => [ 'string', 'max:255'],
          'lastname' => [ 'string', 'max:255'],
          'birthday'=>'',
          'job_title'=>[ 'string', 'max:255'],
          
          'phone_number' => ['regex:/^([0-9\s\-\+\(\)]*)$/','max:18','min:8'],
          'country' => ['string', 'max:255'],
          'region' => ['string', 'max:255'],
          'city' => ['string', 'max:255'],
          'barangay' => ['string', 'max:255'],
          'street' => ['string', 'max:255'],
          'password' => [ 'string', 'min:8', 'confirmed'],
          
          'profile_photo'=>'',
                
        ]);

        if(request('profile_photo')!==null){ 
            $image_path = request('profile_photo')->store('uploads','public');
            $image = Image::make(public_path("storage/{$image_path}"))->resize(309,null,function($constraint){
            $constraint->aspectRatio();
            });
            $image->save();
        }
        else{
            $image_path = $user->profile_photo;
            $data['profile_photo'] = $image_path ;
        }

        $profile = User::find($id);
        
             $profile->firstname = $data['firstname'];
             $profile->lastname = $data['lastname'];
             $profile->birthday = $data['birthday'];
             $profile->job_title = $data['job_title'];
             $profile->phone_number = $data['phone_number'];
             $profile->country = $data['country'];
             $profile->region = $data['region'];
             $profile->city = $data['city'];
             $profile->barangay = $data['barangay'];
             $profile->street = $data['street'];
             $profile->profile_photo = $image_path;
            //dd($image_path); 
            $profile->save();

           return redirect('/user-profile/'.$profile->token_id)->with('status', 'Updated Successfully. Thank you!'); 

    }

    public function editUser($tokenId)
    {
      $profile = User::where('token_id',$tokenId)->firstOrFail();
      
      return view('user.edit-profile',compact('profile'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
