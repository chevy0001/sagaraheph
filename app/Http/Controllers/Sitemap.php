<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class Sitemap extends Controller
{
    

public function sitemap(){

	    $blogs = DB::table('blogs')->get();
		return response()->view('sitemap',compact('blogs'))->header('Content-Type','text/xml');
	}
}
