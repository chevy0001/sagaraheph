<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use Auth;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    public function showResetForm(){


      $token = Str::random(60);

      return view('auth.passwords.reset-form',compact('token'));
    }



    public function reset(Request $request)
    {
      
      $data = request()->validate([

          'password'=>['required','min:8','confirmed'],
          'token'=>['required'],
      ]);


      $user = User::where('token_id', auth()->user()->token_id)->first();
      $user->password = \Hash::make($data['password']);
      $user->update();
      Auth::login($user);

      return redirect('/user/'.auth()->user()->token_id);
    }

}
