<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     return view('admin.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
          'firstname' => ['required', 'string', 'max:255'],
          'lastname' => ['required', 'string', 'max:255'],
          'birthday'=>'',
          'job_title'=>['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
          'phone_number' => ['required','regex:/^([0-9\s\-\+\(\)]*)$/','max:18','min:8'],
          'country' => ['required', 'string', 'max:255'],
          'region' => ['required', 'string', 'max:255'],
          'city' => ['required', 'string', 'max:255'],
          'barangay' => ['required', 'string', 'max:255'],
          'street' => ['required', 'string', 'max:255'],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
          'role'=>'',
          'profile_photo'=>['image','mimes:jpeg,bmp,png,jpg','max:2028'
            ],
                
        ]);

        $image_path = request('profile_photo')->store('uploads','public');
        $image = Image::make(public_path("storage/{$image_path}"))->resize(309,null,function($constraint){
            $constraint->aspectRatio();
        });
        $image->save();

        $user_id = auth()->user()->id;
        //dd($user_id);

        $token_id = Str::random(10);
        auth()->user()->create([
          'firstname' => $data['firstname'],
          'lastname' => $data['lastname'],
          'birthday'=>$data['birthday'],
          'job_title'=> $data['job_title'],
          'phone_number' => $data['phone_number'],
          'email' => $data['email'],
          'phone_number' => $data['phone_number'],
          'country'=> $data['country'],
          'region'=> $data['region'],
          'city'=> $data['city'],
          'barangay'=> $data['barangay'],
          'street'=> $data['street'],
          'password' => Hash::make($data['password']),
          'role' => '0',
          'profile_photo'=>$image_path,
          'token_id'=>$token_id,
          'is_delete' => '0',
          'user_id'=>$user_id,
        ]);
         //\App\Authors::create($data);   
        //dd(request()->all());   
  //Send Email after successful registration    
    //Email codes here 
  //redirect to clients profile
        
  return redirect('/user-profile/'.$token_id)->with('status', 'Member Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show($tokenId)
    {
        $profile = DB::table('users')
                   ->where('users.token_id',$tokenId)
                   ->first();
        $status = DB::table('statuses')
                   ->where('statuses.user_token_id',$tokenId)
                   ->where('upgrade_request','1')
                   ->latest()
                   ->first(); 
                // dd()                   
                   //dd($profile);
        $paymentHistory = DB::table('payment_histories')
                   ->where('payment_histories.client_token_id',$tokenId)
                   ->orderBy('created_at','desc')
                   ->get(); 
        //dd($paymentHistory);                   
                    
        return view('admin.profile',compact('profile','status','paymentHistory'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */

    public function list()
    {
        $users = DB::table('users')->get();
        //dd($users);
        return view('admin.list' ,compact('users'));
    }
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
