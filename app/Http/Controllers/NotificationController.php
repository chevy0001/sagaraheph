<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use DB;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {


     
     if(auth()->user()->role === '1'){
      $notifs = DB::table('users')
              ->rightJoin('notifications','notifications.client_token_id','=','users.token_id')
              ->where('notifications.is_read',0)
              ->latest('notifications.created_at')
              ->paginate(10);
      }

      else{
        $notifs = DB::table('users')
              ->rightJoin('notifications','notifications.client_token_id','=','users.token_id')
              ->where('notifications.is_read',0)
              ->where('notifications.client_token_id',auth()->user()->token_id)
              ->latest('notifications.created_at')
              ->paginate(10);
      }
      

      //dd(auth()->user()->role);
      return view('notification.notif-board', compact('notifs'));  
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}
