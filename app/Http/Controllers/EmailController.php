<?php

namespace App\Http\Controllers;

use App\Email;
use Illuminate\Http\Request;
use Mail;
use DB;
class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()    
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function newsletter()
    {

        $data = request()->validate([
               'emailAddress'=>'required', 
        ]);

        $data['ip'] = \Request::ip();

        $lead = new \App\Email();
        $lead->ip = $data['ip'];
        $lead->name = 'Subscriber';
        $lead->phone_number = 'None';
        $lead->email = $data['emailAddress'];
        $lead->message = 'The client only subscribe to our news letter';

        $lead->save();

        $email = 'sagaraheph@gmail.com';
        $headers = "From: <".$data['emailAddress'].">\r\n";
        $headers.="Reply-To:". $data['emailAddress']."\r\n";
        $headers.="Content-type: text/html\r\n";
        $to = $email;
        $subject = 'Sagarahe Subscription';
        $message = '<html><body>';
        $message.=' Subscription Email: '.$data['emailAddress'];
        $message.='</body></html>';
        
        mail($to,$subject,$message,$headers);

        return back(); 

       
    }

    public function store(Request $request)
    {
        $data = request()->validate([
                'name' => 'required',
                'emailAddress'=>['required'],
                'phoneNo'=>['required','regex:/^([0-9\s\-\+\(\)]*)$/','max:18','min:8'],
                'clientMessage'=>['required'],
                'plan'=>['required'],
                
        ]);
       
        $data['ip'] = \Request::ip();
        
        

        $lead = new \App\Email();
        $lead->ip = $data['ip'];
        $lead->name = $data['name'];
        $lead->phone_number = $data['phoneNo'];
        $lead->email = $data['emailAddress'];
        $lead->message = $data['clientMessage'];
        $lead->plan = $data['plan'];

        $lead->save();

        $email = 'sagaraheph@gmail.com';
        $headers = "From: <".$data['emailAddress'].">\r\n";
        $headers.="Reply-To:". $data['emailAddress']."\r\n";
        $headers.="Content-type: text/html\r\n";
        $to = $email;
        $subject = 'Sagarahe Inquiry';
        $message = '<html><body>';
        $message.= $data['name'].' is requesting for qoute about'.$data['plan'].' <br/>Email: '.$data['emailAddress'].'. Phone number: '. $data['phoneNo'];
        $message.='<br/>Message: '.$data['clientMessage'];
        $message.='</body></html>';
        
        mail($to,$subject,$message,$headers);

        $headersTo = "From: <sagaraheph@gmail.com>\r\n";
        $headersTo.="Reply-To:sagaraheph@gmail.com\r\n";
        $headersTo.="Content-type: text/html\r\n";
        $toTo = $data['emailAddress'];
        $subjectTo = 'Sagarahe Inquiry';
        $messageTo = '<html><body>';
        $messageTo.= 'Hi '.$data['name'].'<br/> Thank you for your inquiry. Our staff will get to you as soon as possible.';
        $messageTo.='</body></html>';

        

        mail($toTo,$subjectTo,$messageTo,$headersTo);
      
        return back()->with('status', 'Submitted Successfully. Our admin will contact you as soon as possible. Thank you!');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function show(Email $email)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function edit(Email $email)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Email $email)
    {
        return view('test');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Email  $email
     * @return \Illuminate\Http\Response
     */
    public function destroy(Email $email)
    {
        //
    }
}
