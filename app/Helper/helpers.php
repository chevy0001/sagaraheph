<?php
if (!function_exists('versioned_asset')) {
    /**
     * Generate a URL to an application asset with a versioned timestamp parameter.
     * @param $path
     * @param null $secure
     * @return string
     */
    function versioned_asset($path, $secure = null)
    {
        $timestamp = @filemtime(public_path($path)) ?: 0;
        return asset($path, $secure) . '?' . $timestamp;
    }
}
?>