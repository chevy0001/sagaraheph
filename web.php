<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...

Route::get('/sagarahestaff/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
	Route::post('/sagarahestaff/register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@reset');

//**************************

Route::get('/', 'HomeController@homepage');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth.basic');
Route::get('/blog', 'HomeController@blog');
Route::get('/blog/1', 'HomeController@blogPost');
Route::get('/services', 'HomeController@services');
Route::get('/services/1', 'HomeController@servicesPost');
Route::get('/pricing', 'HomeController@pricing');
Route::get('/about', 'HomeController@about');
Route::get('/contact', 'HomeController@contact');
Route::get('/get-quote', 'HomeController@getQuote');

Route::group(['middleware' => 'App\Http\Middleware\Admin'], function()
{
	
	Route::post('/store-user', 'ProfileController@store')->name('storeuser');
	Route::get('/register-user', 'ProfileController@create')->name('registeruser')->middleware('auth.basic');
	Route::get('/user-list', 'ProfileController@list')->name('userlist');
	Route::get('/user-profile/{tokenid}', 'ProfileController@show');

	Route::get('/admin/{tokenid}', 'AdminController@index');

	//Edit individual user
	Route::get('/edit/{tokenid}', 'UserController@edit')->name('edit-user');
	Route::patch('/profile-edit/{tokenid}', 'UserController@update')->name('update-user');

	//Status Controller
	Route::get('/add-status/{token_id}', 'StatusController@index')->name('status-dashboard');
	Route::post('/store-status/{token_id}', 'StatusController@store')->name('status-dashboard');
	Route::patch('/update-payment/{tokenid}', 'PaymentHistoryController@update')->name('update-payment');

});
Route::get('/user/{tokenid}', 'UserController@index')->middleware('auth.basic');
Route::get('/add-payment/{tokenid}', 'PaymentHistoryController@index')->middleware('auth.basic');
//receipt
Route::post('/store-reciept/{tokenid}', 'PaymentHistoryController@store')->middleware('auth.basic');
Route::get('/refresh', function () {
  Artisan::call('view:clear');
  return back();
});

Route::get('/storage-link', function () {
  Artisan::call('storage:link');
  return back();
});

Route::get('/db-seed-users', function () {
  Artisan::call('db:seed --class=UserSeeder');
  return back();
});